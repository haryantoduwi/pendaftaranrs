<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= ucwords($global->headline)?> | Administrator</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/select2/select2.min.css">  
  <link rel="stylesheet" href="<?= base_url();?>asset/fontawesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/datatables_new/datatables.css"> 
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/datatables_new/FixedHeader-3.1.4/css/fixedHeader.bootstrap.min.css"> 
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/datatables_new/Responsive-2.2.2/css/responsive.bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/datatables_new/Buttons-1.5.6/css/buttons.bootstrap.min.css">   
 
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/datepicker/datepicker3.css">  
  <link rel="stylesheet" href="<?= base_url();?>asset/plugins/animate-css/animate.css">   
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/sweetalert.css">
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/skins/_all-skins.min.css">
  
  <!--JAVASCRIPT CORE-->
  <script src="<?= base_url();?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?= base_url();?>asset/bootstrap/js/bootstrap.min.js"></script>  
  <script src="<?= base_url();?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <script src="<?= base_url();?>asset/plugins/chartjs/Chart.bundle.js"></script>
  <script src="<?= base_url();?>asset/plugins/chartjs/utils.js"></script>

  <script src="<?= base_url();?>asset/plugins/datatables_new/dataTables.min.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/dataTables.bootstrap.min.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/FixedHeader-3.1.4/js/dataTables.fixedHeader.min.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/Responsive-2.2.2/js/dataTables.responsive.min.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/Responsive-2.2.2/js/responsive.bootstrap.min.js"></script>

  <script src="<?= base_url();?>asset/plugins/datatables_new/Buttons-1.5.6/js/dataTables.buttons.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/Buttons-1.5.6/js/buttons.flash.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/Buttons-1.5.6/js/buttons.html5.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/Buttons-1.5.6/js/buttons.print.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/JSZip-2.5.0/jszip.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/pdfmake-0.1.36/pdfmake.js"></script>
  <script src="<?= base_url();?>asset/plugins/datatables_new/pdfmake-0.1.36/vfs_fonts.js"></script>

  <script src="<?= base_url();?>asset/plugins/select2/select2.full.min.js"></script> 
  <script src="<?= base_url();?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
  <script src="<?= base_url();?>asset/plugins/ckeditor/ckeditor.js"></script>  
  <script src="<?= base_url();?>asset/dist/js/sweetalert.min.js"></script>
  <script src="<?= base_url();?>asset/dist/js/jquery.priceformat.min.js"></script>  
  <script src="<?= base_url();?>asset/dist/js/jquery.validate.js"></script>
  <script src="<?= base_url();?>asset/plugins/bootstrap-notify/bootstrap-notify.js"></script>
</head>

<script type="text/javascript">
  $(document).ready(function(){
      var table = $('#test').DataTable( {
          responsive: true,
          dom: 'Bfrtip',
          buttons: [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ]         
      } );
   
      //new $.fn.dataTable.FixedHeader( table );    
  })
</script>