<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Pendaftaranpoli extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
	}
	//VARIABEL
	private $master_tabel="daftar"; //Mendefinisikan Nama Tabel
	private $id="daftar_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="frontend/pendaftaranpoli/"; //Mendefinisikan url controller
	private $default_view="frontend/poli/"; //Mendefinisiakn defaul view
	private $view="template/webfrontend"; //Mendefinisikan Tamplate Root
	private $path='./upload/';

	private function global_set($data){
		$data=array(
			'menu'=>'poli',//Seting menu yang aktif
			'submenu_menu'=>'poli',
			'menu_submenu'=>false,
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa  fa-book",
			'view'=>"views/frontend/poli/index.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=poli, bentuk obyek
		//$data['menu']=poli, array bentuk biasa
	}
	private function hapus_file($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->id=>$id)),
		);
		$file=$this->Crud->read($query)->row();
		if($file->poli_file){
			unlink($this->path.$file->poli_file);
		}
	}
	public function index($daftar=null)
	{
		$global_set=array(
			'headline'=>'pendaftaran poli',
			'url'=>$this->default_url,
			'submenu'=>false,
		);
		if($daftar){
			$daftar=$daftar;
		}else{
			$daftar=false;
		}
		$global=$this->global_set($global_set);
		if($this->input->post('kunjungan_idpasien')){
		//CEK SUBMIT DATAt('kunjungan_idpasien')){
			//PROSES SIMPAN
			$data=array(
				'kunjungan_idpasien'=>$this->input->post('kunjungan_idpasien'),
				'kunjungan_tanggal'=>date('Y-m-d',strtotime($this->input->post('kunjungan_tanggal'))),
				'kunjungan_tersimpan'=>date('Y-m-d'),
				'kunjungan_iddokter'=>$this->input->post('kunjungan_iddokter'),
				'kunjungan_status'=>0,
				//SET DEFAULT ANTRIAN 4 STATUS MENUNGGU
				'kunjungan_antrian_status'=>4,
			);
			// GENERATE NO PENDAFTARAN
			$kodependaftaran='SELECT RIGHT(kunjungan_kode,4) AS kode from kunjunganpasien ORDER BY kode DESC LIMIT 1';
			$kode=$this->Crud->hardcode($kodependaftaran);
			$dt=array(
				'kode'=>$kode,
				'kodedokter'=>$data['kunjungan_iddokter'],
			);
			$generetkodependaftaran=$this->kodependaftaran($dt);
			$data['kunjungan_kode']=$generetkodependaftaran;
			//PROSES PEMBUATAN NO RM
			// $rm='SELECT RIGHT(poli_rm,4) AS kode from poli ORDER BY kode DESC LIMIT 1';
			// $rm=$this->Crud->hardcode($rm);
			// $dt=array(
			// 	'norm'=>$rm,
			// );
			// $fin_rm=$this->norm($dt);
			// $data['poli_rm']=$fin_rm;
			// END PROSES PEMBUATAN NO RM
			
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['poli_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		//$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		//redirect(site_url($this->default_url));
			// 		$dt['error']=$this->upload->display_errors();
			// 		return $this->output->set_output(json_encode($dt));
			// 		exit();
			// 	}
			// }			
			
			$query=array(
				'data'=>$data,
				'tabel'=>'kunjunganpasien',
			);
			$insert=$this->Crud->insert_returnid($query);
			$this->notifiaksi(true);
			redirect(base_url($this->default_url).'index/'.md5($insert));
			// return $this->output->set_output(json_encode($dt));

			//print_r($data);
			// exit();
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu(0),
				'data'=>false,
			);
			//JIKA SUDAH DAFTAR TULIS ULANG VARIABEL DATA
			if($daftar){
				$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,b.jampraktek_mulai,b.jampraktek_selesai,c.poli_nama FROM dokter a 
					JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
					JOIN poli c ON c.poli_id=a.dokter_idpoli
					JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
					JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien WHERE md5(kunjungan_id)='$daftar'";
				$data['data']=$this->Crud->hardcode($query)->row();
			}
			//$this->viewdata($daftar);			
			$this->load->view($this->view,$data);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>'poli',
			'url'=>$this->default_url,
			'submenu'=>false,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>$this->id,'orderby'=>'DESC'),
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		//$this->load->view($this->default_view.'tabel',$data);	
		$this->load->view($this->default_view.'formdaftar',$data);			
	}
	public function ceknorm(){
		$norm=trim($this->input->post('norm'));
		$tgllahir=date('Y-m-d',strtotime($this->input->post('tgllahir')));
		$global_set=array(
			'headline'=>'pendaftaran poli',
			'url'=>$this->default_url,
			'submenu'=>false,
		);	
		$global=$this->global_set($global_set);	
		$query=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_rm'=>$norm),array('pendaftaran_tgllahir'=>$tgllahir)),
		);
		$poli=array(
			'tabel'=>'poli',
			'order'=>array('kolom'=>'poli_nama','orderby'=>'ASC'),
		);	
		$dokter=array(
			'tabel'=>'dokter',
			'order'=>array('kolom'=>'dokter_nama','orderby'=>'ASC'),
		);						
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->row(),
			'poli'=>$this->Crud->read($poli)->result(),
			'dokter'=>$this->Crud->read($dokter)->result(),
		);
		//$this->viewdata($data['data']);
		$this->load->view($this->default_view.'listdatadiri2',$data);
	}
	public function jenispendaftaran(){
		$pilihan=$this->input->post('pilihan');
		$poli=array(
			'tabel'=>'poli',
			'order'=>array('kolom'=>'poli_nama','orderby'=>'ASC'),
		);	
		$dokter=array(
			'tabel'=>'dokter',
			'order'=>array('kolom'=>'dokter_nama','orderby'=>'ASC'),
		);		
		$data=array(
			'pilihan'=>$pilihan,
			'poli'=>$this->Crud->read($poli)->result(),
			'dokter'=>$this->Crud->read($dokter)->result(),			
		);
		//echo $pilihan;
		$this->load->view($this->default_view.'pilihanpendaftaran',$data);
	}
	public function getatributdokter(){
		$iddokter=$this->input->post('iddokter');
		$atribut="SELECT a.*,b.dokter_id,b.dokter_nama,c.poli_id,c.poli_nama,c.poli_kuota FROM jampraktek a 
			JOIN dokter b ON b.dokter_idjampraktek=a.jampraktek_id
			JOIN poli c ON c.poli_id=b.dokter_idpoli
			WHERE b.dokter_id=$iddokter";	
		$res_atribut=$this->Crud->hardcode($atribut)->row();
		$data=array(
			'atribut'=>$res_atribut,
		);	
		//print_r($data);
		$this->load->view($this->default_view.'atributdokter',$data);
	}
	public function getjadwal(){
		$idpoli=trim($this->input->post('idpoli'));
		$poli="SELECT a.* FROM jampraktek a 
			LEFT JOIN dokter b ON b.dokter_idjampraktek=a.jampraktek_id
			GROUP BY a.jampraktek_nama";
		$datapoli="SELECT a.* FROM poli a WHERE a.poli_id=$idpoli";			
		$jadwal="SELECT a.*,b.dokter_id,b.dokter_nama,c.poli_id,c.poli_nama,c.poli_kuota FROM jampraktek a 
			JOIN dokter b ON b.dokter_idjampraktek=a.jampraktek_id
			JOIN poli c ON c.poli_id=b.dokter_idpoli
			WHERE c.poli_id=$idpoli";		
		// $poli=array(
		// 	'select'=>'a.*,b.*',
		// 	'tabel'=>'poli a',
		// 	'join'=>array(array('tabel'=>'jampraktek b','ON'=>'b.jampraktek_idpoli=a.poli_id','jenis'=>'INNER')),
		// 	'where'=>array(array('a.poli_id'=>$idpoli)),
		// );		
		$data=array(
			'poli'=>$this->Crud->hardcode($datapoli)->row(),
			'jadwal'=>$this->Crud->hardcode($jadwal)->result(),
			'data'=>$this->Crud->hardcode($poli)->result(),
		);
		//$this->viewdata($data);
		$this->load->view($this->default_view.'getjadwal',$data);
	}	
	public function kuota(){
		$idjampraktek=trim($this->input->post('idjampraktek'));
		$idpoli=trim($this->input->post('idpoli'));
		$pilihtanggal=date('Y-m-d', strtotime($this->input->post('pilihtanggal')));
		$query="SELECT count(a.daftar_idpoli) AS jumlah,b.poli_kuota,a.daftar_idpoli,c.jampraktek_nama,c.jampraktek_mulai,c.jampraktek_selesai FROM daftar a
				JOIN poli b ON b.poli_id=a.daftar_idpoli
				JOIN jampraktek c ON c.jampraktek_id=a.daftar_idjam
				WHERE b.poli_id=$idpoli AND a.daftar_idjam=$idjampraktek AND a.daftar_tanggal='$pilihtanggal'
				GROUP BY a.daftar_idpoli,c.jampraktek_mulai,c.jampraktek_selesai
				";
		$poli=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_idjampraktek'=>$idjampraktek)),
		);		
		// $poli=array(
		// 	'select'=>'a.*,b.*',
		// 	'tabel'=>'poli a',
		// 	'join'=>array(array('tabel'=>'jampraktek b','ON'=>'b.jampraktek_idpoli=a.poli_id','jenis'=>'INNER')),
		// 	'where'=>array(array('a.poli_id'=>$idpoli)),
		// );		
		$data=array(
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//$this->viewdata($data);
		$this->load->view($this->default_view.'getkuota',$data);
	}		
	public function edit(){
		$global_set=array(
			'headline'=>'edit data',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('poli_nama')){
			//PROSES SIMPAN
			$data=array(
				'poli_nama'=>$this->input->post('poli_nama'),
				'poli_date'=>date('Y-m-d',strtotime($this->input->post('poli_date'))),
				'poli_keterangan'=>$this->input->post('poli_keterangan'),
			);
			$file='fileupload';
			if($_FILES[$file]['name']){
				if($this->fileupload($this->path,$file)){
					$this->hapus_file($id);
					$file=$this->upload->data('file_name');
					$data['poli_file']=$file;
					//print_r($data);
				}else{
					//$this->session->set_flashdata('error',$this->upload->display_errors());
					//redirect(site_url($this->default_url));
					$dt['upload']=$this->upload->display_errors();
					return $this->output->set_output(json_encode($dt));
					exit();	
				}
			}			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->id=>$id),
			);
			$update=$this->Crud->update($query);
			if($update){
				$dt['success']='update data berhasil';
			}else{
				$dt['error']='input data error';
			}
			return $this->output->set_content_type('aplication/json')->set_output(json_encode($dt));			
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->id=>$id))
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->default_view.'edit',$data);
		}			
	}	
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'add data',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);	
		$global=$this->global_set($global_set);
		$data=array(
			'global'=>$global,
			);
		$this->load->view($this->default_view.'add',$data);		
	}	
	public function hapus(){
		$id=$this->input->post('id');
		$this->hapus_file($id);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->id=>$id),
		);
		$delete=$this->Crud->delete($query);
		if($delete){
			$dt['success']='hapus data berhasil';
		}else{
			$dt['error']='input data error';
			$dt['msg']=$delete;
		}
		return $this->output->set_output(json_encode($dt));	
	}
	public function cetakformulir($id){
		// $query=array(
		// 	'tabel'=>'kunjunganpasien',
		// 	'where'=>array(array('kunjungan_id'=>$id))
		// );
		$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,e.pendaftaran_foto,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,b.jampraktek_mulai,b.jampraktek_selesai,c.poli_nama FROM dokter a 
			JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
			JOIN poli c ON c.poli_id=a.dokter_idpoli
			JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
			JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien WHERE md5(kunjungan_id)='$id'";
		$res=$this->Crud->hardcode($query)->row();		
		
		$data=array(
			'data'=>$res,
			'judul'=>'pendaftaran poli mandiri'
		);
		// $pdfdt=array(
		// 	'view'=>$this->load->view($this->default_view.'cetakformulirpendaftaran',$data,true),
		// 	'judul'=>'Form data diri',
		// );
		// $this->prosescetak($pdfdt);
		// $this->viewdata($data);
		$this->load->view($this->default_view.'cetakformulirpendaftaran2',$data,false);
	}
}
