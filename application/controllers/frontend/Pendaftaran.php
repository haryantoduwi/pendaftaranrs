<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Pendaftaran extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
	}
	//VARIABEL
	private $master_tabel="pendaftaran"; //Mendefinisikan Nama Tabel
	private $id="pendaftaran_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="frontend/pendaftaran/"; //Mendefinisikan url controller
	private $default_view="frontend/pendaftaran/"; //Mendefinisiakn defaul view
	private $view="template/webfrontend"; //Mendefinisikan Tamplate Root
	private $path='./upload/';

	private function global_set($data){
		$data=array(
			'menu'=>'pendaftaran',//Seting menu yang aktif
			'submenu_menu'=>'pasien baru',
			'menu_submenu'=>false,
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa  fa-book",
			'view'=>"views/frontend/pendaftaran/index.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=pendaftaran, bentuk obyek
		//$data['menu']=pendaftaran, array bentuk biasa
	}
	private function hapus_file($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->id=>$id)),
		);
		$file=$this->Crud->read($query)->row();
		if($file->pendaftaran_file){
			unlink($this->path.$file->pendaftaran_file);
		}
	}
	public function index()
	{
		$global_set=array(
			'headline'=>'pendaftaran',
			'url'=>$this->default_url,
			'submenu'=>false,
		);
		$global=$this->global_set($global_set);
		//CEK SUBMIT DATA
		if($this->input->post('pendaftaran_nama')){
			//PROSES SIMPAN
			$data=array(
				'pendaftaran_nama'=>$this->input->post('pendaftaran_nama'),
				'pendaftaran_tgllahir'=>date('Y-m-d',strtotime($this->input->post('pendaftaran_tgllahir'))),
				'pendaftaran_noktp'=>$this->input->post('pendaftaran_noktp'),
				'pendaftaran_alamat'=>$this->input->post('pendaftaran_alamat'),
				'pendaftaran_tempatlahir'=>$this->input->post('pendaftaran_tempatlahir'),
				'pendaftaran_notlp'=>$this->input->post('pendaftaran_notlp'),
				'pendaftaran_terdaftar'=>date('Y-m-d'),
				'pendaftaran_jeniskelamin'=>$this->input->post('pendaftaran_jeniskelamin'),
				/////////////////////////////////////////////////////////////////////////
				'pendaftaran_username'=>$this->input->post('pendaftaran_username'),
				'pendaftaran_password'=>$this->input->post('pendaftaran_password'),
				'pendaftaran_status'=>0,
			);
			//PROSES PEMBUATAN NO RM
			$rm='SELECT RIGHT(pendaftaran_rm,4) AS kode from pendaftaran ORDER BY kode DESC LIMIT 1';
			$rm=$this->Crud->hardcode($rm);
			$dt=array(
				'norm'=>$rm,
			);
			$fin_rm=$this->norm($dt);
			$data['pendaftaran_rm']=$fin_rm;
			// END PROSES PEMBUATAN NO RM
			
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['pendaftaran_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		//$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		//redirect(site_url($this->default_url));
			// 		$dt['error']=$this->upload->display_errors();
			// 		return $this->output->set_output(json_encode($dt));
			// 		exit();
			// 	}
			// }			
			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			if($insert){
				$dt['success']='input data berhasil';
			}else{
				$dt['error']='input data error';
			}
			return $this->output->set_output(json_encode($dt));

			// print_r($data);
			// exit();
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->view,$data);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>'pendaftaran',
			'url'=>$this->default_url,
			'submenu'=>false,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>$this->id,'orderby'=>'DESC'),
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		//$this->load->view($this->default_view.'tabel',$data);	
		$this->load->view($this->default_view.'formdaftar',$data);			
	}
	public function edit(){
		$global_set=array(
			'headline'=>'edit data',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('pendaftaran_nama')){
			//PROSES SIMPAN
			$data=array(
				'pendaftaran_nama'=>$this->input->post('pendaftaran_nama'),
				'pendaftaran_date'=>date('Y-m-d',strtotime($this->input->post('pendaftaran_date'))),
				'pendaftaran_keterangan'=>$this->input->post('pendaftaran_keterangan'),
			);
			$file='fileupload';
			if($_FILES[$file]['name']){
				if($this->fileupload($this->path,$file)){
					$this->hapus_file($id);
					$file=$this->upload->data('file_name');
					$data['pendaftaran_file']=$file;
					//print_r($data);
				}else{
					//$this->session->set_flashdata('error',$this->upload->display_errors());
					//redirect(site_url($this->default_url));
					$dt['upload']=$this->upload->display_errors();
					return $this->output->set_output(json_encode($dt));
					exit();	
				}
			}			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->id=>$id),
			);
			$update=$this->Crud->update($query);
			if($update){
				$dt['success']='update data berhasil';
			}else{
				$dt['error']='input data error';
			}
			return $this->output->set_content_type('aplication/json')->set_output(json_encode($dt));			
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->id=>$id))
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->default_view.'edit',$data);
		}			
	}	
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'add data',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);	
		$global=$this->global_set($global_set);
		$data=array(
			'global'=>$global,
			);
		$this->load->view($this->default_view.'add',$data);		
	}	
	public function hapus(){
		$id=$this->input->post('id');
		$this->hapus_file($id);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->id=>$id),
		);
		$delete=$this->Crud->delete($query);
		if($delete){
			$dt['success']='hapus data berhasil';
		}else{
			$dt['error']='input data error';
			$dt['msg']=$delete;
		}
		return $this->output->set_output(json_encode($dt));	
	}

}
