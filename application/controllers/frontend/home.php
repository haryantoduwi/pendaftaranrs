<?php
/*

	ADA FUNGSI LOGIN DISINI

*/
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Home extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
	}
	//VARIABEL
	private $master_tabel="pendaftaran"; //Mendefinisikan Nama Tabel
	private $id="pendaftaran_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="frontend/home/"; //Mendefinisikan url controller
	private $default_view="frontend/home/"; //Mendefinisiakn defaul view
	private $view="template/webfrontend"; //Mendefinisikan Tamplate Root
	private $path='./upload/';

	private function global_set($data){
		$data=array(
			'menu'=>'home',//Seting menu yang aktif
			'submenu_menu'=>$data['submenu'],
			'menu_submenu'=>false,
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa fa-home",
			'view'=>"views/frontend/home/container.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=home, bentuk obyek
		//$data['menu']=home, array bentuk biasa
	}

	public function index()
	{
		if($this->session->userdata('user_login')==true){
			if($this->session->userdata('user_level')==99){
				redirect(site_url('dashboard/user'));
			}else if($this->session->userdata('user_level')==1){
				redirect(site_url('dashboard/admin'));
			}
		}		
		$global_set=array(
			'headline'=>'Home',
			'url'=>$this->default_url,
			'submenu'=>'dashboard',
		);
		$global=$this->global_set($global_set);
		if($this->input->post('user_username')){
			//PROSES SIMPAN
			$data=array(
				'pendaftaran_username'=>$this->input->post('user_username'),
				'pendaftaran_password'=>$this->input->post('user_password'),
			);
			$query=array(
				// 'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array(array('pendaftaran_username'=>$data['pendaftaran_username']),array('pendaftaran_password'=>$data['pendaftaran_password'])),
			);
			$read=$this->Crud->read($query);
			if($read->num_rows()==1){
				$user=$read->row();
				$sess=array(
					'user_id'=>$user->pendaftaran_id,
					'user_nama'=>$user->pendaftaran_nama,
					'user_terdaftar'=>$user->pendaftaran_terdaftar,
					'user_status'=>$user->pendaftaran_status,
					'user_foto'=>$user->pendaftaran_foto,
					'user_login'=>true,
					'user_level'=>99,
				);
				$this->session->set_userdata($sess);
				redirect(site_url("dashboard/user"));
			}else{
				$this->session->set_flashdata('error','user tidak ditemukan');
				redirect(site_url($this->default_url));
			}
			// $this->notifiaksi($insert);
			// redirect(site_url($this->default_url));
			//print_r($data);
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->viewdata($data);			
			$this->load->view($this->view,$data);
			//print_r($data['data']);
		}
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url($this->default_url));		
	}
	// public function tabel(){
	// 	$global_set=array(
	// 		'headline'=>false,
	// 		'url'=>$this->default_url,
	// 		'submenu'=>false,
	// 	);
	// 	//LOAD FUNCTION GLOBAL SET
	// 	$global=$this->global_set($global_set);		
	// 	//PROSES TAMPIL DATA
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 	);
	// 	$data=array(
	// 		'global'=>$global,
	// 		'data'=>$this->Crud->read($query)->result(),
	// 	);
	// 	//$this->viewdata($data);
	// 	$this->load->view($this->default_view.'tabel',$data);		
	// }
	// public function edit(){
	// 	$global_set=array(
	// 		'headline'=>'edit data',
	// 		'url'=>$this->default_url.'/edit',
	// 	);
	// 	$global=$this->global_set($global_set);
	// 	$id=$this->input->post('id');
	// 	if($this->input->post('submit')){
	// 		//PROSES SIMPAN
	// 		$data=array(
	// 			'user_nama'=>$this->input->post('user_nama'),
	// 			'user_terdaftar'=>date('Y-m-d',strtotime($this->input->post('user_terdaftar'))),
	// 			'user_username'=>$this->input->post('user_username'),
	// 			'user_password'=>$this->input->post('user_password'),
	// 			'user_level'=>$this->input->post('user_level'),
	// 		);
	// 		$query=array(
	// 			'data'=>$data,
	// 			'tabel'=>$this->master_tabel,
	// 			'where'=>array($this->id=>$id),
	// 		);
	// 		$update=$this->Crud->update($query);
	// 		$this->notifiaksi($update);
	// 		redirect(site_url($this->default_url));
	// 	}else{
	// 		$query=array(
	// 			'tabel'=>$this->master_tabel,
	// 			'where'=>array(array($this->id=>$id))
	// 		);
	// 		$data=array(
	// 			'data'=>$this->Crud->read($query)->row(),
	// 			'global'=>$global,
	// 			'menu'=>$this->menu(0),
	// 		);
	// 		//$this->viewdata($data);			
	// 		$this->load->view($this->default_view.'edit',$data);
	// 	}			
	// }	
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'add data',
	// 		'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
	// 	);	
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		'global'=>$global,
	// 		);
	// 	$this->load->view($this->default_view.'add',$data);		
	// }	
	// public function hapus($id){
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array('user_id'=>$id),
	// 	);
	// 	$delete=$this->Crud->delete($query);
	// 	$this->notifiaksi($delete);
	// 	redirect(site_url($this->default_url));
	// }

}
