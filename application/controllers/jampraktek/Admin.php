<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		// if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')!=1) ){
		// 	redirect(site_url('login'));
		// }
		$this->cekadmin();

	}
	//VARIABEL
	private $master_tabel="jampraktek";
	private $default_url="jampraktek/admin/";
	private $default_view="jampraktek/admin/";
	private $view="template/backend";
	private $master_id="jampraktek_id";

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'master',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/jampraktek/admin/index.php",
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>false,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'jam praktek',
			'headline'=>'daftar jampraktek',
			'url'=>'jampraktek/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'jampraktek_nama'=>$this->input->post('jampraktek_nama'),
				//'jampraktek_idpoli'=>$this->input->post('jampraktek_idpoli'),
				//'jampraktek_iddokter'=>$this->input->post('jampraktek_iddokter'),
				'jampraktek_mulai'=>date('h:m:s',strtotime($this->input->post('jampraktek_mulai'))),
				'jampraktek_selesai'=>date('h:m:s',strtotime($this->input->post('jampraktek_selesai'))),
			);
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['jampraktek_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			// $poli=array(
			// 	'tabel'=>'poli',
			// 	'order'=>array('kolom'=>'poli_nama','orderby'=>'ASC'),
			// );
			// $dokter=array(
			// 	'tabel'=>'dokter',
			// 	'order'=>array('kolom'=>'dokter_nama','orderby'=>'ASC'),
			// );			
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),
				//'poli'=>$this->Crud->read($poli)->result(),
				//'dokter'=>$this->Crud->read($dokter)->result(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'jampraktek',
	// 		'where'=>array(array('jampraktek_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'jampraktek/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'jampraktek_id','orderby'=>'DESC'),
			
			);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}

	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail jampraktek',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$id))
		);
		$data=array(
			'data'=>$this->Crud->read($query)->row(),
			'global'=>$global,
		);
		$this->load->view($this->default_view.'detail',$data);		
	}
	public function cetakformulir(){
		$user_id=$this->user_id;
		$query=array(
			'tabel'=>'jampraktek',
			'where'=>array(array('jampraktek_id'=>$user_id))
		);
		$result=$this->Crud->read($query)->row();
		$data=array(
			'data'=>$result,
		);
		$pdfdt=array(
			'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
			'judul'=>'Form data diri',
		);
		$this->prosescetak($pdfdt);
		$this->viewdata($data);
		//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	}	
	// public function edit(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'edit data',
	// 		'url'=>$this->default_url.'edit',
	// 	);
	// 	$global=$this->global_set($global_set);
	// 	$id=$this->input->post('id');
	// 	if($this->input->post('submit')){
	// 		$data=array(
	// 			'jampraktek_nama'=>$this->input->post('jampraktek_nama'),
	// 			'jampraktek_tgllahir'=>date('Y-m-d',strtotime($this->input->post('jampraktek_tgllahir'))),
	// 			'jampraktek_noktp'=>$this->input->post('jampraktek_noktp'),
	// 			'jampraktek_alamat'=>$this->input->post('jampraktek_alamat'),
	// 			'jampraktek_tempatlahir'=>$this->input->post('jampraktek_tempatlahir'),
	// 			'jampraktek_notlp'=>$this->input->post('jampraktek_notlp'),
	// 			'jampraktek_jeniskelamin'=>$this->input->post('jampraktek_jeniskelamin'),
	// 			/////////////////////////////////////////////////////////////////////////
	// 			'jampraktek_username'=>$this->input->post('jampraktek_username'),
	// 			'jampraktek_password'=>$this->input->post('jampraktek_password'),
	// 		);
	// 		// $file='fileupload';
	// 		// if($_FILES[$file]['name']){
	// 		// 	if($this->fileupload($this->path,$file)){
	// 		// 		$file=$this->upload->data('file_name');
	// 		// 		$data['jampraktek_file']=$file;
	// 		// 		//print_r($data);
	// 		// 	}else{
	// 		// 		$this->session->set_flashdata('error',$this->upload->display_errors());
	// 		// 		redirect(site_url($this->default_url));
	// 		// 	}
	// 		// }			
	// 		$query=array(
	// 			'data'=>$data,
	// 			'where'=>array($this->id=>$id),
	// 			'tabel'=>$this->master_tabel,
	// 			);
	// 		$update=$this->Crud->update($query);
	// 		$this->notifiaksi($update);
	// 		redirect(site_url($this->default_url));
	// 	}else{
	// 		$query=array(
	// 			'tabel'=>$this->master_tabel,
	// 			'where'=>array(array('jampraktek_id'=>$id)),
	// 		);		
	// 		$data=array(
	// 			'data'=>$this->Crud->read($query)->row(),
	// 			'global'=>$global,
	// 		);
	// 		//print_r($data['data']);
	// 		$this->load->view($this->default_view.'editpasien',$data);
	// 	}
	// }			
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'crud',
	// 		'url'=>'crud/admin/', //AKAN DIREDIRECT KE INDEX
	// 	);
	// 	$user=array(
	// 		'tabel'=>"user",
	// 		'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
	// 		);		
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		//'user'=>$this->Crud->read($user)->result(),
	// 		'global'=>$global,
	// 		);

	// 	$this->load->view($this->default_view.'add',$data);		
	// }	
	public function hapus($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->master_id=>$id),
		);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}	
}
