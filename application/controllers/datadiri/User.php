<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class User extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')==1) ){
			redirect(site_url('login'));
		}

	}
	//VARIABEL
	private $master_tabel="pendaftaran";
	private $default_url="datadiri/User/";
	private $default_view="datadiri/user/";
	private $view="template/frontend";
	private $master_id="pendaftaran_id";

	private $pathfoto="./upload/foto/";
	private $pathktp="./upload/ktp/";
	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'data diri',
			'submenu_menu'=>false,
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/datadiri/user/index.php",
			'add'=>false,
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	private function hapus_file($dt){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$dt['id'])),
		);
		$data=$this->Crud->read($query)->row();
		$kolom=$dt['kolom'];
		$file=$data->$kolom;
		if($file){
			unlink($dt['path'].$file);
			return true;
		}else{
			return false;
		}
	}
	public function index()
	{
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data diri',
			'url'=>'datadiri/user/',
		);	
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			$id=$this->input->post('id');
			$data=array(
				'pendaftaran_nama'=>$this->input->post('pendaftaran_nama'),
				'pendaftaran_tgllahir'=>date('Y-m-d',strtotime($this->input->post('pendaftaran_tgllahir'))),
				'pendaftaran_noktp'=>$this->input->post('pendaftaran_noktp'),
				'pendaftaran_alamat'=>$this->input->post('pendaftaran_alamat'),
				'pendaftaran_tempatlahir'=>$this->input->post('pendaftaran_tempatlahir'),
				'pendaftaran_notlp'=>$this->input->post('pendaftaran_notlp'),
				'pendaftaran_terdaftar'=>date('Y-m-d'),
				'pendaftaran_jeniskelamin'=>$this->input->post('pendaftaran_jeniskelamin'),
				/////////////////////////////////////////////////////////////////////////
				'pendaftaran_username'=>$this->input->post('pendaftaran_username'),
				////////////////////////////////////////////////////////////////////////
				'pendaftaran_namawali'=>$this->input->post('pendaftaran_namawali'),
				'pendaftaran_alamatwali'=>$this->input->post('pendaftaran_alamatwali'),
			);
			if($this->input->post('pendaftaran_password')) $data['pendaftaran_password']=$this->input->post('pendaftaran_password');

			$file='pendaftaran_foto';
			if($_FILES[$file]['name']){
				if($this->fotoupload($this->pathfoto,$file)){
					$dt=array(
						'id'=>$id,
						'kolom'=>'pendaftaran_foto',
						'path'=>$this->pathfoto,
					);
					$this->hapus_file($dt);
					$file=$this->upload->data('file_name');
					$data['pendaftaran_foto']=$file;
					//print_r($data);				
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}
			$filektp='pendaftaran_ktp';
			if($_FILES[$filektp]['name']){
				if($this->fotoupload($this->pathktp,$filektp)){
					$dt=array(
						'id'=>$id,
						'kolom'=>'pendaftaran_ktp',
						'path'=>$this->pathktp,
					);
					$this->hapus_file($dt);					
					$ktp=$this->upload->data('file_name');
					$data['pendaftaran_ktp']=$ktp;
					//print_r($data);
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}	
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->master_id=>$id),
			);
			$insert=$this->Crud->update($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//$this->viewdata($data);											
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);			
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);			
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'datadiri kunjungan',
			'url'=>'datadiri/user',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
			JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
			JOIN poli c ON c.poli_id=a.dokter_idpoli
			JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
			JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien
			WHERE d.kunjungan_idpasien=$this->user_id";
		$data=array(
			'global'=>$global,
			'subheadline'=>'datadiri kunjungan berobat pasien',
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}

	// public function detail(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'detail kunjungan',
	// 		'url'=>$this->default_url,
	// 	);
	// 	$global=$this->global_set($global_set);		
	// 	$id=$this->input->post('id');
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array(array($this->master_id=>$id))
	// 	);
	// 	$data=array(
	// 		'data'=>$this->Crud->read($query)->row(),
	// 		'global'=>$global,
	// 	);
	// 	$this->load->view($this->default_view.'detail',$data);		
	// }
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'kunjungan',
	// 		'where'=>array(array('kunjungan_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }		
}
