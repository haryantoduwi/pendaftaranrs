<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->level_id=$this->session->userdata('user_id');
		$this->level_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')>=5) ){
			redirect(site_url('login'));
		}

	}
	//VARIABEL
	private $master_tabel="level";
	private $default_url="level/admin/";
	private $default_view="level/admin/";
	private $view="template/backend";
	private $master_id="level_id";

	//HARDCODE LEVEL level
	//private $level_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'setting',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-levels",
			'view'=>"views/level/admin/index.php",
			'add'=>false,
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'level',
			'headline'=>'daftar level user',
			'url'=>'level/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'level_nama'=>$this->input->post('level_nama'),
				'level_keterangan'=>$this->input->post('level_keterangan'),
				'level_created_at'=>date('Y-m-d'),
			);
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['level_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data);
		}else{
	
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->level_level),


			);
			//$exmenu=$this->menubackend_dev(2);
			//$this->viewdata($exmenu);	
			$this->load->view($this->view,$data);
		}
	}
	public function cari(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'laporan level pasien',
			'url'=>'level/admin/',
		);
		$global=$this->global_set($global_set);	
		$tglmulai=$this->input->post('tglmulai');
		$tglselesai=$this->input->post('tglselesai');
		
		$tglmulai=date('Y-m-d',strtotime($tglmulai));
		$tglselesai=date('Y-m-d',strtotime($tglselesai));
		$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
			JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
			JOIN poli c ON c.poli_id=a.dokter_idpoli
			JOIN levelpasien d ON d.level_iddokter=a.dokter_id
			JOIN pendaftaran e ON e.pendaftaran_id=d.level_idpasien
			WHERE d.level_tanggal BETWEEN '$tglmulai' AND '$tglselesai'
			ORDER BY d.level_tanggal DESC";
		$data=array(
			'global'=>$global,
			'subheadline'=>'data pesien yang mendaftar mandiri, yang sudah berstatus aktif',
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//echo $tglmulai;
		$this->load->view($this->default_view.'tabel',$data);
	}	
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'pengguna sistem',
			'url'=>'level/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'level_nama','orderby'=>'asc'),
		);
		$data=array(
			'global'=>$global,
			'subheadline'=>'data level pengguna sistem',
			'data'=>$this->Crud->read($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'level pengguna',
			'url'=>'level/admin/', //AKAN DIREDIRECT KE INDEX
		);
		$user=array(
			'tabel'=>"user",
			'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
			);		
		$global=$this->global_set($global_set);
		$data=array(
			//'user'=>$this->Crud->read($user)->result(),
			'global'=>$global,
			);

		$this->load->view($this->default_view.'add',$data);		
	}	
	public function hapus($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->master_id=>$id),
		);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}
	protected function remove_akses_level_submenu($id){
		$q_allmenu="SELECT * FROM menu WHERE menu_is_mainmenu!=0";
		$get_allmenu=$this->Crud->hardcode($q_allmenu)->result();
		$res_submenu=array();
		foreach ($get_allmenu as $index => $row) {
			$res_submenu[$index]=$row;
			$arr_submenu=explode(',', $row->menu_akses_level);
			if(($key=array_search($id, $arr_submenu))!==false){
				unset($arr_submenu[$key]);
			}
			$imp_submenu=implode(',',$arr_submenu);
			$update_dt=array(
				'menu_akses_level'=>$imp_submenu,
			);
			$q_update=array(
				'tabel'=>'menu',
				'data'=>$update_dt,
				'where'=>array('menu_id'=>$row->menu_id),
			);
			$update=$this->Crud->update($q_update);
			//$res_submenu[$index]->levelakses=$imp_submenu		
		}
		return $update;
	}
	protected function remove_akses_level_menu($id){
		$q_allmenu="SELECT * FROM menu WHERE menu_is_mainmenu=0";
		$get_allmenu=$this->Crud->hardcode($q_allmenu)->result();
		$res_submenu=array();
		foreach ($get_allmenu as $index => $row) {
			$res_submenu[$index]=$row;
			$arr_submenu=explode(',', $row->menu_akses_level);
			if(($key=array_search($id, $arr_submenu))!==false){
				unset($arr_submenu[$key]);
			}
			$imp_submenu=implode(',',$arr_submenu);
			$update_dt=array(
				'menu_akses_level'=>$imp_submenu,
			);
			$q_update=array(
				'tabel'=>'menu',
				'data'=>$update_dt,
				'where'=>array('menu_id'=>$row->menu_id),
			);
			$update=$this->Crud->update($q_update);	
			if(!$update){
				$this->notifiaksi($update);
				redirect(site_url($this->default_url));					
			}						
		}		
	}	
	public function assign(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'edit data',
			'url'=>$this->default_url.'assign',
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'level_nama'=>$this->input->post('level_nama'),
				'level_id'=>$this->input->post('id'),
				'level_assign'=>$this->input->post('assign'),
				'level_assignsubmenu'=>$this->input->post('assignsubmenu'),
			);
			$res_menu=array();
			//MENU MAIN
			if($data['level_assign']){
				$this->remove_akses_level_menu($data['level_id']);
				foreach ($data['level_assign'] AS $index => $row) {
					$res_menu[$index]=$row;
					$q_menu="SELECT * FROM menu WHERE menu_id=$res_menu[$index]";
					$menu=$this->Crud->hardcode($q_menu)->row();
					//AMBIL DATA AKSES MENU DAN CONVERT KE ARRAY
					$aksesmenu=$menu->menu_akses_level;
					$aksesarray=explode(',',$aksesmenu );
					//TAMBAHKAN ID DATA LEVEL
					
					if(!in_array($id, $aksesarray)){
						//INSSRT LEVEL ID DI ARRAY AKSES LEVEL
						$add=array_push($aksesarray, $data['level_id']);
					}else{
						$add=$aksesarray;
					}
					//IMPLODE ARRAY
					$aksesmenu=implode(',',$aksesarray);
					$update_menu=array(
						'menu_akses_level'=>$aksesmenu,
					);
					$dt_update=array(
						'tabel'=>'menu',
						'data'=>$update_menu,
						'where'=>array('menu_id'=>$res_menu[$index]),
					);
					$update=$this->Crud->update($dt_update);
					if(!$update){
						$this->notifiaksi($update);
						redirect(site_url($this->default_url));					
					}						
					// $res_menu[$index]=$menu;
					// $res_menu[$index]->update=$aksesarray;
				}
				// $this->viewdata($dt_update);
				// exit();
			}else{
				$this->remove_akses_level_menu($data['level_id']);
			}
			//SUB MENU
			if($data['level_assignsubmenu']){
				$this->remove_akses_level_submenu($data['level_id']);
				foreach ($data['level_assignsubmenu'] AS $index => $row) {
					$res_menu[$index]=$row;
					$q_menu="SELECT * FROM menu WHERE menu_id=$res_menu[$index]";
					$menu=$this->Crud->hardcode($q_menu)->row();
					//AMBIL DATA AKSES MENU DAN CONVERT KE ARRAY
					$aksesmenu=$menu->menu_akses_level;
					$aksesarray=explode(',',$aksesmenu );
					//TAMBAHKAN ID DATA LEVEL
					if(!in_array($id, $aksesarray)){
						//INSSRT LEVEL ID DI ARRAY AKSES LEVEL
						$add=array_push($aksesarray, $data['level_id']);
					}else{
						$add=$aksesarray;
					}
					//IMPLODE ARRAY
					$aksesmenu=implode(',',$aksesarray);
					$update_menu=array(
						'menu_akses_level'=>$aksesmenu,
					);
					$dt_update=array(
						'tabel'=>'menu',
						'data'=>$update_menu,
						'where'=>array('menu_id'=>$res_menu[$index]),
					);
					$update=$this->Crud->update($dt_update);
				}
				$this->notifiaksi($update);
				redirect(site_url($this->default_url));

				// $this->viewdata($update_menu);
				// $this->viewdata($menu);				
			}else{
				$update=$this->remove_akses_level_submenu($data['level_id']);
				$this->notifiaksi($update);
				redirect(site_url($this->default_url));
			}
		}else{
			//AMBIL LEVEL MENU
			$query=array(
					'tabel'=>$this->master_tabel,
					'where'=>array(array('level_id'=>$id)),
				);
			$menu="SELECT * FROM menu WHERE menu_status=1 AND menu_is_mainmenu=0 AND menu_akses_level!=0 AND menu_akses_level!=99  ORDER BY menu_nama";
			$res_menu=$this->Crud->hardcode($menu)->result();
			$assign=array();
			foreach ($res_menu as $index => $row) {
				$assign[$index]=$row;
				$submenu="SELECT * FROM menu WHERE menu_status=1 AND menu_is_mainmenu=$row->menu_id ORDER BY menu_nama";
				$res_submenu=$this->Crud->hardcode($submenu)->result();
				if(count($res_submenu)>=1){
					//AMBIL DATA SUBMENU
					$arrsubmenu=array();
					foreach ($res_submenu as $indexs => $rows) {
						$arrsubmenu[$indexs]=$rows;
						$expsubmenu=explode(',', $rows->menu_akses_level);
						$ceksubmenu=in_array($id, $expsubmenu);
						if($ceksubmenu){
							$arrsubmenu[$indexs]->akseslevel=true;	
						}else{
							$arrsubmenu[$indexs]->akseslevel=false;	
						}						
					}
					$assign[$index]->submenu=$arrsubmenu;
				}else{
					$assign[$index]->submenu=false;
				}

				//AMBIL DATA MENU UTAMA
				$aksesarray=explode(',', $row->menu_akses_level);
				$cekmenu=in_array($id, $aksesarray);
				$assign[$index]->cekmenu=$aksesarray;
				if($cekmenu){
					$assign[$index]->akseslevel=true;
				}else{
					$assign[$index]->akseslevel=false;
				}
				
			}									
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'menu'=>$assign,
				'global'=>$global,
			);
			$this->load->view($this->default_view.'assign',$data);
			//$this->viewdata($assign);
			//$this->viewdata($assign);
		}
	}				
}
