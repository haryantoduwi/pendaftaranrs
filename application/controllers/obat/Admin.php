<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')!=1) ){
			redirect(site_url('login'));
		}

	}
	//VARIABEL
	private $master_tabel="pendaftaran";
	private $default_url="pasienbaru/admin/";
	private $default_view="pasienbaru/admin/";
	private $view="template/backend";
	private $master_id="pendaftaran_id";

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'obat',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/pasienbaru/admin/index.php",
			'detail'=>true,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'obat',
			'headline'=>'daftar obat',
			'url'=>'pasienbaru/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			// $data=array(
			// 	'nama'=>$this->input->post('nama'),
			// 	'tgllahir'=>date('Y-m-d',strtotime($this->input->post('tgllahir'))),
			// 	'nomerhp'=>$this->input->post('nomerhp'),
			// 	'desa'=>$this->input->post('desa'),
			// );
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['pendaftaran_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			// $query=array(
			// 	'data'=>$data,
			// 	'tabel'=>$this->master_tabel,
			// );
			// $insert=$this->Crud->insert($query);
			// $this->notifiaksi($insert);
			// redirect(site_url($this->default_url));
			// print_r($data['menu']);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'pendaftaran',
	// 		'where'=>array(array('pendaftaran_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'pasienbaru/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'pendaftaran_id','orderby'=>'DESC'),
			'where'=>array(array('pendaftaran_status'=>0)),
			);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function aktivasi($id){
		$id=$id;
		//CEK AKTIFASI, JIKA AKTIF UPDATE DATA FALSE
		$cek=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_id'=>$id)),
		);
		$cek_res=$this->Crud->read($cek)->row();
		if($cek_res->pendaftaran_status==1){
			$status=0;
		}else{
			$status=1;
		}
		
		$data=array(
			'pendaftaran_status'=>$status,
		);
		$query=array(
			'tabel'=>'pendaftaran',
			'data'=>$data,
			'where'=>array('pendaftaran_id'=>$id),
		);
		$update=$this->Crud->update($query);
		redirect(site_url($this->default_url));
	}
	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail pendaftaran',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$id))
		);
		$data=array(
			'data'=>$this->Crud->read($query)->row(),
			'global'=>$global,
		);
		$this->load->view($this->default_view.'detail',$data);		
	}
	public function cetakformulir(){
		$user_id=$this->user_id;
		$query=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_id'=>$user_id))
		);
		$result=$this->Crud->read($query)->row();
		$data=array(
			'data'=>$result,
		);
		$pdfdt=array(
			'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
			'judul'=>'Form data diri',
		);
		$this->prosescetak($pdfdt);
		$this->viewdata($data);
		//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	}	
	public function edit(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'edit data',
			'url'=>$this->default_url.'edit',
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'pendaftaran_nama'=>$this->input->post('pendaftaran_nama'),
				'pendaftaran_tgllahir'=>date('Y-m-d',strtotime($this->input->post('pendaftaran_tgllahir'))),
				'pendaftaran_noktp'=>$this->input->post('pendaftaran_noktp'),
				'pendaftaran_alamat'=>$this->input->post('pendaftaran_alamat'),
				'pendaftaran_tempatlahir'=>$this->input->post('pendaftaran_tempatlahir'),
				'pendaftaran_notlp'=>$this->input->post('pendaftaran_notlp'),
				'pendaftaran_jeniskelamin'=>$this->input->post('pendaftaran_jeniskelamin'),
				/////////////////////////////////////////////////////////////////////////
				'pendaftaran_username'=>$this->input->post('pendaftaran_username'),
				'pendaftaran_password'=>$this->input->post('pendaftaran_password'),
			);
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['pendaftaran_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }			
			$query=array(
				'data'=>$data,
				'where'=>array($this->id=>$id),
				'tabel'=>$this->master_tabel,
				);
			$update=$this->Crud->update($query);
			$this->notifiaksi($update);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('pendaftaran_id'=>$id)),
			);		
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
			);
			//print_r($data['data']);
			$this->load->view($this->default_view.'editpasien',$data);
		}
	}			
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'crud',
	// 		'url'=>'crud/admin/', //AKAN DIREDIRECT KE INDEX
	// 	);
	// 	$user=array(
	// 		'tabel'=>"user",
	// 		'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
	// 		);		
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		//'user'=>$this->Crud->read($user)->result(),
	// 		'global'=>$global,
	// 		);

	// 	$this->load->view($this->default_view.'add',$data);		
	// }	


	// public function hapus($id){
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array($this->id=>$id),
	// 	);
	// 	$delete=$this->Crud->delete($query);
	// 	$this->notifiaksi($delete);
	// 	redirect(site_url($this->default_url));
	// }	
}
