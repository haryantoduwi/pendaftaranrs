<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class User extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')==1) ){
			redirect(site_url('login'));
		}

	}
	//VARIABEL
	private $master_tabel="kunjunganpasien";
	private $default_url="riwayat/User/";
	private $default_view="riwayat/user/";
	private $view="template/frontend";
	private $master_id="kunjungan_id";

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'riwayat',
			'submenu_menu'=>false,
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/riwayat/user/index.php",
			'add'=>false,
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'jam praktek',
			'headline'=>'daftar kunjungan',
			'url'=>'riwayat/user/',
		);	
		$global=$this->global_set($global_set);
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$this->user_id))
		);
		// $poli=array(
		// 	'tabel'=>'poli',
		// 	'order'=>array('kolom'=>'poli_nama','orderby'=>'ASC'),
		// );
		// $dokter=array(
		// 	'tabel'=>'dokter',
		// 	'order'=>array('kolom'=>'dokter_nama','orderby'=>'ASC'),
		// );			
		$data=array(
			'global'=>$global,
			'menu'=>$this->menubackend($this->user_level),
			'data'=>$this->Crud->read($query)->row(),
			//'poli'=>$this->Crud->read($poli)->result(),
			//'dokter'=>$this->Crud->read($dokter)->result(),

		);
		//$this->viewdata($data);	
		$this->load->view($this->view,$data);
		//print_r($data['menu'][1]->submenu);
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'riwayat kunjungan',
			'url'=>'riwayat/user',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
			JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
			JOIN poli c ON c.poli_id=a.dokter_idpoli
			JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
			JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien
			WHERE d.kunjungan_idpasien=$this->user_id 
			ORDER BY d.kunjungan_tanggal DESC ";
		$data=array(
			'global'=>$global,
			'subheadline'=>'riwayat kunjungan berobat pasien',
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}

	// public function detail(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'detail kunjungan',
	// 		'url'=>$this->default_url,
	// 	);
	// 	$global=$this->global_set($global_set);		
	// 	$id=$this->input->post('id');
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array(array($this->master_id=>$id))
	// 	);
	// 	$data=array(
	// 		'data'=>$this->Crud->read($query)->row(),
	// 		'global'=>$global,
	// 	);
	// 	$this->load->view($this->default_view.'detail',$data);		
	// }
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'kunjungan',
	// 		'where'=>array(array('kunjungan_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }		
}
