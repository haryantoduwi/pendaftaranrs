<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		if(($this->session->userdata('user_login')!=true) || ($this->session->userdata('user_level')>=5) ){
			redirect(site_url('login'));
		}

	}
	//VARIABEL
	private $master_tabel="user";
	private $default_url="user/admin/";
	private $default_view="user/admin/";
	private $view="template/backend";
	private $master_id="user_id";

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'setting',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/user/admin/index.php",
			'add'=>false,
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'user',
			'headline'=>'daftar user',
			'url'=>'user/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'user_nama'=>$this->input->post('user_nama'),
				'user_username'=>$this->input->post('user_username'),
				'user_password'=>$this->input->post('user_password'),
				'user_level'=>$this->input->post('user_level'),
				'user_terdaftar'=>date('Y-m-d'),
			);
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['user_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			$level=array(
				'tabel'=>'level',
				'order'=>array('kolom'=>'level_nama','orderby'=>'ASC'),
			);		
			$data=array(
				'global'=>$global,
				//'menu'=>$this->menubackend($this->user_level),
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),
				'level'=>$this->Crud->read($level)->result(),

			);
			//$exmenu=$this->menubackend_dev(2);
			//$this->viewdata($exmenu);	
			$this->load->view($this->view,$data);
		}
	}
	public function cari(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'laporan user pasien',
			'url'=>'user/admin/',
		);
		$global=$this->global_set($global_set);	
		$tglmulai=$this->input->post('tglmulai');
		$tglselesai=$this->input->post('tglselesai');
		
		$tglmulai=date('Y-m-d',strtotime($tglmulai));
		$tglselesai=date('Y-m-d',strtotime($tglselesai));
		$query="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
			JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
			JOIN poli c ON c.poli_id=a.dokter_idpoli
			JOIN userpasien d ON d.user_iddokter=a.dokter_id
			JOIN pendaftaran e ON e.pendaftaran_id=d.user_idpasien
			WHERE d.user_tanggal BETWEEN '$tglmulai' AND '$tglselesai'
			ORDER BY d.user_tanggal DESC";
		$data=array(
			'global'=>$global,
			'subheadline'=>'data pesien yang mendaftar mandiri, yang sudah berstatus aktif',
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//echo $tglmulai;
		$this->load->view($this->default_view.'tabel',$data);
	}	
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'user',
	// 		'where'=>array(array('user_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'pengguna sistem',
			'url'=>'user/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'user_nama','orderby'=>'desc'),
		);
		$data=array(
			'global'=>$global,
			'subheadline'=>'data user pengguna sistem',
			'data'=>$this->Crud->read($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}

	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail user',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$id))
		);
		$data=array(
			'data'=>$this->Crud->read($query)->row(),
			'global'=>$global,
		);
		$this->load->view($this->default_view.'detail',$data);		
	}
	public function cetakformulir(){
		$user_id=$this->user_id;
		$query=array(
			'tabel'=>'user',
			'where'=>array(array('user_id'=>$user_id))
		);
		$result=$this->Crud->read($query)->row();
		$data=array(
			'data'=>$result,
		);
		$pdfdt=array(
			'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
			'judul'=>'Form data diri',
		);
		$this->prosescetak($pdfdt);
		$this->viewdata($data);
		//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	}	
	// public function edit(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'edit data',
	// 		'url'=>$this->default_url.'edit',
	// 	);
	// 	$global=$this->global_set($global_set);
	// 	$id=$this->input->post('id');
	// 	if($this->input->post('submit')){
	// 		$data=array(
	// 			'user_nama'=>$this->input->post('user_nama'),
	// 			'user_tgllahir'=>date('Y-m-d',strtotime($this->input->post('user_tgllahir'))),
	// 			'user_noktp'=>$this->input->post('user_noktp'),
	// 			'user_alamat'=>$this->input->post('user_alamat'),
	// 			'user_tempatlahir'=>$this->input->post('user_tempatlahir'),
	// 			'user_notlp'=>$this->input->post('user_notlp'),
	// 			'user_jeniskelamin'=>$this->input->post('user_jeniskelamin'),
	// 			/////////////////////////////////////////////////////////////////////////
	// 			'user_username'=>$this->input->post('user_username'),
	// 			'user_password'=>$this->input->post('user_password'),
	// 		);
	// 		// $file='fileupload';
	// 		// if($_FILES[$file]['name']){
	// 		// 	if($this->fileupload($this->path,$file)){
	// 		// 		$file=$this->upload->data('file_name');
	// 		// 		$data['user_file']=$file;
	// 		// 		//print_r($data);
	// 		// 	}else{
	// 		// 		$this->session->set_flashdata('error',$this->upload->display_errors());
	// 		// 		redirect(site_url($this->default_url));
	// 		// 	}
	// 		// }			
	// 		$query=array(
	// 			'data'=>$data,
	// 			'where'=>array($this->id=>$id),
	// 			'tabel'=>$this->master_tabel,
	// 			);
	// 		$update=$this->Crud->update($query);
	// 		$this->notifiaksi($update);
	// 		redirect(site_url($this->default_url));
	// 	}else{
	// 		$query=array(
	// 			'tabel'=>$this->master_tabel,
	// 			'where'=>array(array('user_id'=>$id)),
	// 		);		
	// 		$data=array(
	// 			'data'=>$this->Crud->read($query)->row(),
	// 			'global'=>$global,
	// 		);
	// 		//print_r($data['data']);
	// 		$this->load->view($this->default_view.'editpasien',$data);
	// 	}
	// }			
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'crud',
	// 		'url'=>'crud/admin/', //AKAN DIREDIRECT KE INDEX
	// 	);
	// 	$user=array(
	// 		'tabel'=>"user",
	// 		'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
	// 		);		
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		//'user'=>$this->Crud->read($user)->result(),
	// 		'global'=>$global,
	// 		);

	// 	$this->load->view($this->default_view.'add',$data);		
	// }
	public function aktivasi($id){
		$id=$id;
		//CEK AKTIFASI, JIKA AKTIF UPDATE DATA FALSE
		$cek=array(
			'tabel'=>'userpasien',
			'where'=>array(array('user_id'=>$id)),
		);
		$cek_res=$this->Crud->read($cek)->row();
		if($cek_res->user_status==1){
			$status=0;
		}else{
			$status=1;
		}
		
		$data=array(
			'user_status'=>$status,
		);
		$query=array(
			'tabel'=>'userpasien',
			'data'=>$data,
			'where'=>array('user_id'=>$id),
		);
		$update=$this->Crud->update($query);
		redirect(base_url($this->default_url));
	}
	public function suspend($id){
		$id=$id;
		$data=array(
			'user_status'=>99,
		);
		$query=array(
			'tabel'=>'userpasien',
			'data'=>$data,
			'where'=>array('user_id'=>$id),
		);
		$update=$this->Crud->update($query);
		redirect(base_url($this->default_url));
	}			
	public function hapus($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array($this->master_id=>$id),
		);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}	
}
