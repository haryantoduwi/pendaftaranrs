<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		$this->cekadmin();
		// if(($this->session->userdata('user_login')!=true) || ($this->user_level >=5)){
		// 	redirect(site_url('login'));
		// }

	}
	//VARIABEL
	private $master_tabel="pendaftaran";
	private $default_url="dashboard/admin/";
	private $default_view="dashboard/admin/";
	private $view="template/backend";
	private $master_id="pendaftaran_id";

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'dashboard',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-dashboard",
			'view'=>"views/dashboard/admin/index.php",
			'detail'=>false,
			'edit'=>true,
			'delete'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>false,
			'headline'=>'dashboard',
			'url'=>'dashboard/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			// $data=array(
			// 	'nama'=>$this->input->post('nama'),
			// 	'tgllahir'=>date('Y-m-d',strtotime($this->input->post('tgllahir'))),
			// 	'nomerhp'=>$this->input->post('nomerhp'),
			// 	'desa'=>$this->input->post('desa'),
			// );
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['pendaftaran_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			// $query=array(
			// 	'data'=>$data,
			// 	'tabel'=>$this->master_tabel,
			// );
			// $insert=$this->Crud->insert($query);
			// $this->notifiaksi($insert);
			// redirect(site_url($this->default_url));
			// print_r($data['menu']);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			$tgl=date('Y-m-d');
			$kunjungan="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
				JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
				JOIN poli c ON c.poli_id=a.dokter_idpoli
				JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
				JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien
				WHERE d.kunjungan_tanggal='$tgl' AND kunjungan_status=1";
			$pendaftaran="SELECT * FROM pendaftaran WHERE pendaftaran_status=0";
			$kunjunganpasien="SELECT d.*,e.pendaftaran_nama,e.pendaftaran_rm,a.dokter_nik,a.dokter_nama,b.jampraktek_nama,c.poli_nama FROM dokter a 
				JOIN jampraktek b ON b.jampraktek_id=a.dokter_idjampraktek
				JOIN poli c ON c.poli_id=a.dokter_idpoli
				JOIN kunjunganpasien d ON d.kunjungan_iddokter=a.dokter_id
				JOIN pendaftaran e ON e.pendaftaran_id=d.kunjungan_idpasien
				ORDER BY d.kunjungan_tanggal DESC LIMIT 5";				
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),
				'jumkunjunganperhari'=>$this->Crud->hardcode($kunjungan)->num_rows(),
				'jumpendaftaranperhari'=>$this->Crud->hardcode($pendaftaran)->num_rows(),
				'kunjunganpasien'=>$this->Crud->hardcode($kunjunganpasien)->result(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function cetakformulir(){
		$user_id=$this->user_id;
		$query=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_id'=>$user_id))
		);
		$result=$this->Crud->read($query)->row();
		$data=array(
			'data'=>$result,
		);
		$pdfdt=array(
			'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
			'judul'=>'Form data diri',
		);
		$this->prosescetak($pdfdt);
		$this->viewdata($data);
		//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'dashboard/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'pendaftaran_id','orderby'=>'DESC'),
			);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		print_r($data['data']);
		//$this->load->view($this->default_view.'tabel',$data);		
	}
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'crud',
	// 		'url'=>'crud/admin/', //AKAN DIREDIRECT KE INDEX
	// 	);
	// 	$user=array(
	// 		'tabel'=>"user",
	// 		'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
	// 		);		
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		//'user'=>$this->Crud->read($user)->result(),
	// 		'global'=>$global,
	// 		);

	// 	$this->load->view($this->default_view.'add',$data);		
	// }	
	// public function edit(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'edit data',
	// 		'url'=>'crud/admin/edit',
	// 	);
	// 	$global=$this->global_set($global_set);
	// 	$id=$this->input->post('id');
	// 	if($this->input->post('submit')){
	// 		$data=array(
	// 			'nama'=>$this->input->post('nama'),
	// 			'tgllahir'=>date('Y-m-d',strtotime($this->input->post('tgllahir'))),
	// 			'nomerhp'=>$this->input->post('nomerhp'),
	// 			'desa'=>$this->input->post('desa'),
	// 			//'pendaftaran_tersimpan'=>date('Y-m-d')
	// 		);
	// 		// $file='fileupload';
	// 		// if($_FILES[$file]['name']){
	// 		// 	if($this->fileupload($this->path,$file)){
	// 		// 		$file=$this->upload->data('file_name');
	// 		// 		$data['pendaftaran_file']=$file;
	// 		// 		//print_r($data);
	// 		// 	}else{
	// 		// 		$this->session->set_flashdata('error',$this->upload->display_errors());
	// 		// 		redirect(site_url($this->default_url));
	// 		// 	}
	// 		// }			
	// 		$query=array(
	// 			'data'=>$data,
	// 			'where'=>array($this->id=>$id),
	// 			'tabel'=>$this->master_tabel,
	// 			);
	// 		$update=$this->Crud->update($query);
	// 		$this->notifiaksi($update);
	// 		redirect(site_url($this->default_url));
	// 	}else{
	// 		$query=array(
	// 			'tabel'=>$this->master_tabel,
	// 			'where'=>array('id'=>$id),
	// 		);
	// 		$user=array(
	// 			'tabel'=>"user",
	// 			'order'=>array('kolom'=>'user_id','orderby'=>'ASC'),
	// 			);			
	// 		$data=array(
	// 			'data'=>$this->Crud->read($query)->row(),
	// 			'global'=>$global,
	// 		);
	// 		//print_r($data['data']);
	// 		$this->load->view($this->default_view.'edit',$data);
	// 	}
	// }	
	// public function detail(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'detail pendaftaran',
	// 		'url'=>'admin/pendaftaran/edit',
	// 	);
	// 	$global=$this->global_set($global_set);		
	// 	$id=$this->input->post('id');
	// 	$query=array(
	// 		'select'=>'a.pendaftaran_id,a.pendaftaran_userid,a.pendaftaran_tgl,a.pendaftaran_judul,a.pendaftaran_keterangan,a.pendaftaran_file,a.pendaftaran_tersimpan,b.user_username,b.user_email',
	// 		'tabel'=>'pendaftaran a',
	// 		'join'=>array(array('tabel'=>'user b','ON'=>'b.user_id=a.pendaftaran_userid','jenis'=>'inner')),
	// 		'order'=>array('kolom'=>'a.pendaftaran_id','orderby'=>'ASC'),
	// 		'where'=>array('a.pendaftaran_id'=>$id),
	// 	);
	// 	$data=array(
	// 		'data'=>$this->Crud->join($query)->row(),
	// 		'global'=>$global,
	// 	);
	// 	$this->load->view($this->default_view.'detail',$data);		
	// }
	// public function hapus($id){
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array($this->id=>$id),
	// 	);
	// 	$delete=$this->Crud->delete($query);
	// 	$this->notifiaksi($delete);
	// 	redirect(site_url($this->default_url));
	// }	
}
