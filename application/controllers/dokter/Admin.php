<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		$this->cekadmin();

	}
	//VARIABEL
	private $master_tabel="dokter";
	private $default_url="dokter/admin/";
	private $default_view="dokter/admin/";
	private $view="template/backend";
	private $master_id="dokter_id";

	private $path_fotodokter='./upload/fotodokter';

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'master',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-users",
			'view'=>"views/dokter/admin/index.php",
			'detail'=>false,
			'edit'=>false,
			'delete'=>false,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>'dokter',
			'headline'=>'daftar dokter',
			'url'=>'dokter/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'dokter_nama'=>$this->input->post('dokter_nama'),
				'dokter_tgllahir'=>date('Y-m-d',strtotime($this->input->post('dokter_tgllahir'))),
				'dokter_nik'=>$this->input->post('dokter_nik'),
				'dokter_jeniskelamin'=>$this->input->post('dokter_jeniskelamin'),
				'dokter_tempatlahir'=>$this->input->post('dokter_tempatlahir'),
				'dokter_notlp'=>$this->input->post('dokter_notlp'),
				'dokter_alamat'=>$this->input->post('dokter_alamat'),
				////////////////////////////////
				'dokter_idjampraktek'=>$this->input->post('dokter_idjampraktek'),
				'dokter_idpoli'=>$this->input->post('dokter_poli'),
			);
			$file='fileupload';
			if($_FILES[$file]['name']){
				if($this->fotoupload($this->path_fotodokter,$file)){
					$file=$this->upload->data('file_name');
					$data['dokter_foto']=$file;
					//print_r($data);
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//print_r($data['data']);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'pendaftaran',
	// 		'where'=>array(array('pendaftaran_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'data',
			'url'=>'dokter/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'dokter_id','orderby'=>'DESC'),
			);
		$data=array(
			'global'=>$global,
			
			'data'=>$this->Crud->read($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function aktivasi($id){
		$id=$id;
		//CEK AKTIFASI, JIKA AKTIF UPDATE DATA FALSE
		$cek=array(
			'tabel'=>'pendaftaran',
			'where'=>array(array('pendaftaran_id'=>$id)),
		);
		$cek_res=$this->Crud->read($cek)->row();
		if($cek_res->pendaftaran_status==1){
			$status=0;
		}else{
			$status=1;
		}
		
		$data=array(
			'pendaftaran_status'=>$status,
		);
		$query=array(
			'tabel'=>'pendaftaran',
			'data'=>$data,
			'where'=>array('pendaftaran_id'=>$id),
		);
		$update=$this->Crud->update($query);
		redirect(site_url($this->default_url));
	}
	public function detail(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'detail pendaftaran',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);		
		$id=$this->input->post('id');
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array($this->master_id=>$id))
		);
		$data=array(
			'data'=>$this->Crud->read($query)->row(),
			'global'=>$global,
		);
		$this->load->view($this->default_view.'detail',$data);		
	}
	// public function cetakformulir(){
	// 	$user_id=$this->user_id;
	// 	$query=array(
	// 		'tabel'=>'pendaftaran',
	// 		'where'=>array(array('pendaftaran_id'=>$user_id))
	// 	);
	// 	$result=$this->Crud->read($query)->row();
	// 	$data=array(
	// 		'data'=>$result,
	// 	);
	// 	$pdfdt=array(
	// 		'view'=>$this->load->view($this->default_view.'cetakdatadiri',$data,true),
	// 		'judul'=>'Form data diri',
	// 	);
	// 	$this->prosescetak($pdfdt);
	// 	$this->viewdata($data);
	// 	//$this->load->view($this->default_view.'cetakdatadiri',$data,false);
	// }	
	public function edit(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'edit data',
			'url'=>$this->default_url.'edit',
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'pendaftaran_nama'=>$this->input->post('pendaftaran_nama'),
				'pendaftaran_tgllahir'=>date('Y-m-d',strtotime($this->input->post('pendaftaran_tgllahir'))),
				'pendaftaran_noktp'=>$this->input->post('pendaftaran_noktp'),
				'pendaftaran_alamat'=>$this->input->post('pendaftaran_alamat'),
				'pendaftaran_tempatlahir'=>$this->input->post('pendaftaran_tempatlahir'),
				'pendaftaran_notlp'=>$this->input->post('pendaftaran_notlp'),
				'pendaftaran_jeniskelamin'=>$this->input->post('pendaftaran_jeniskelamin'),
				/////////////////////////////////////////////////////////////////////////
				'pendaftaran_username'=>$this->input->post('pendaftaran_username'),
				'pendaftaran_password'=>$this->input->post('pendaftaran_password'),
				////////////////////////////////////////////////////////////////////////

			);
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fileupload($this->path,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['pendaftaran_file']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }			
			$query=array(
				'data'=>$data,
				'where'=>array($this->id=>$id),
				'tabel'=>$this->master_tabel,
				);
			$update=$this->Crud->update($query);
			$this->notifiaksi($update);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('pendaftaran_id'=>$id)),
			);		
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
			);
			//print_r($data['data']);
			$this->load->view($this->default_view.'editpasien',$data);
		}
	}			
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'Add Dokter',
			'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
		);
		$user=array(
			'tabel'=>"user",
			'order'=>array('kolom'=>'user_id','orderby'=>'DESC'),
			);	
		$poli=array(
			'tabel'=>'poli',
			'order'=>array('kolom'=>'poli_nama','orderby'=>'ASC'),
		);
		$jampraktek=array(
			'tabel'=>'jampraktek',
			'order'=>array('kolom'=>'jampraktek_mulai','orderby'=>'ASC'),
		);					
		$global=$this->global_set($global_set);
		$data=array(
			//'user'=>$this->Crud->read($user)->result(),
			'global'=>$global,
			'poli'=>$this->Crud->read($poli)->result(),
			'jampraktek'=>$this->Crud->read($jampraktek)->result(),			
			);

		$this->load->view($this->default_view.'add',$data);		
	}	


	// public function hapus($id){
	// 	$query=array(
	// 		'tabel'=>$this->master_tabel,
	// 		'where'=>array($this->id=>$id),
	// 	);
	// 	$delete=$this->Crud->delete($query);
	// 	$this->notifiaksi($delete);
	// 	redirect(site_url($this->default_url));
	// }	
}
