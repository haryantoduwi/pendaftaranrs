<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* 
////////////////////////////////////////////////
			//// README \\\\\
STATUS ANTRIAN 
0 : Sudah dipanggil
1 : Pengembangan
2 : Pengembangan
3 : Pengambangan
4 : Menunggu
5 : Tidak Hadir saat dipanggil/lewati

//////////////////////////////////////////////////
*/


//include controller master 
include APPPATH.'controllers/Master.php';
class Admin extends Master {
	public function __construct(){
		parent::__construct();
		$this->user_id=$this->session->userdata('user_id');
		$this->user_level=$this->session->userdata('user_level');
		$this->load->model('Crud');
		$this->cekadmin();

	}
	//VARIABEL
	private $master_tabel="dokter";
	private $default_url="antrian/admin/";
	private $default_view="antrian/admin/";
	private $view="template/backend";
	private $master_id="dokter_id";

	private $path_fotodokter='./upload/fotodokter';

	//HARDCODE LEVEL USER
	//private $user_level=99;

	private function global_set($data){
		$data=array(
			'menu'=>'antrian',
			'submenu_menu'=>$data['submenu'],
			'headline'=>$data['headline'],
			'url'=>$data['url'],
			'ikon'=>"fa fa-random",
			'view'=>"views/antrian/admin/index.php",
			'detail'=>false,
			'edit'=>false,
			'delete'=>false,
			'aktivasi'=>true,
		);
		return (object)$data;
	}	
	
	public function index()
	{
		$global_set=array(
			'submenu'=>false,
			'headline'=>'antiran pasien',
			'url'=>'antrian/admin/',
		);
				
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			// $data=array(
			// 	'dokter_nama'=>$this->input->post('dokter_nama'),
			// 	'dokter_tgllahir'=>date('Y-m-d',strtotime($this->input->post('dokter_tgllahir'))),
			// 	'dokter_nik'=>$this->input->post('dokter_nik'),
			// 	'dokter_jeniskelamin'=>$this->input->post('dokter_jeniskelamin'),
			// 	'dokter_tempatlahir'=>$this->input->post('dokter_tempatlahir'),
			// 	'dokter_notlp'=>$this->input->post('dokter_notlp'),
			// 	'dokter_alamat'=>$this->input->post('dokter_alamat'),
			// 	////////////////////////////////
			// 	'dokter_idjampraktek'=>$this->input->post('dokter_idjampraktek'),
			// 	'dokter_idpoli'=>$this->input->post('dokter_poli'),
			// );
			// $file='fileupload';
			// if($_FILES[$file]['name']){
			// 	if($this->fotoupload($this->path_fotodokter,$file)){
			// 		$file=$this->upload->data('file_name');
			// 		$data['dokter_foto']=$file;
			// 		//print_r($data);
			// 	}else{
			// 		$this->session->set_flashdata('error',$this->upload->display_errors());
			// 		redirect(site_url($this->default_url));
			// 	}
			// }
			// $query=array(
			// 	'data'=>$data,
			// 	'tabel'=>$this->master_tabel,
			// );
			// $insert=$this->Crud->insert($query);
			// $this->notifiaksi($insert);
			// redirect(site_url($this->default_url));
			//print_r($data['data']);
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->master_id=>$this->user_id))
			);
			
			$data=array(
				'global'=>$global,
				'menu'=>$this->menubackend($this->user_level),
				'data'=>$this->Crud->read($query)->row(),

			);
			//$this->viewdata($data);	
			$this->load->view($this->view,$data);
			//print_r($data['menu'][1]->submenu);
		}
	}
	public function downloadberkas($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'antrian',
			'url'=>'antrian/admin/',
		);
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query="SELECT a.*,b.* FROM dokter a JOIN poli b ON b.poli_id=a.dokter_idpoli ORDER BY dokter_nama ASC";
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->hardcode($query)->result(),
		);
		//print_r($data['data']);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	protected function prosesantr($id){
		//$id adalah kunjungan id dokter
		$date=date('Y-m-d');
		$query=array(
			'tabel'=>'kunjunganpasien',
			'where'=>array(array('kunjungan_iddokter'=>$id),array('kunjungan_antrian_status'=>4),array('kunjungan_tanggal'=>$date)),
			'order'=>array('kolom'=>'kunjungan_id','orderby'=>'ASC'),
		);
		$antrian_all=array(
			'tabel'=>'kunjunganpasien',
			'where'=>array(array('kunjungan_iddokter'=>$id),array('kunjungan_tanggal'=>$date)),
			'order'=>array('kolom'=>'kunjungan_id','orderby'=>'ASC'),
		);		
		$dokter=array(
			'tabel'=>'dokter',
			'where'=>array(array('dokter_id'=>$id)),
		);
		$get_antrian=$this->Crud->read($query)->result();
		if(!$get_antrian){
			$kode_antrian_sekarang='-';
			$res_total_antrian_hari_ini=0;
			$nama_antrian_sekarang='-';
			////////////////
			$id_antrian=false;
		}else{
			// $query_id_antrian_sekarang="SELECT MIN(kunjungan_id) as antrian_sekarang FROM kunjunganpasien WHERE kunjungan_tanggal='$date' AND kunjungan_antrian_status=4 AND kunjungan_iddokter=$id AND LIMIT 1";
			$query_id_antrian_sekarang="SELECT MIN(a.kunjungan_id) as antrian_sekarang, a.kunjungan_kode,b.pendaftaran_nama FROM kunjunganpasien a JOIN pendaftaran b ON b.pendaftaran_id=a.kunjungan_idpasien
				WHERE a.kunjungan_tanggal='$date' AND a.kunjungan_antrian_status=4 AND a.kunjungan_iddokter=$id LIMIT 1";
			$id_antrian_sekarang=$this->Crud->hardcode($query_id_antrian_sekarang)->row();
			$id_antrian=$id_antrian_sekarang->antrian_sekarang;

			////
			$query_antrian_sekarang="SELECT a.* FROM kunjunganpasien a WHERE a.kunjungan_id=$id_antrian AND kunjungan_antrian_status=4";
			$antrian_sekarang=$this->Crud->hardcode($query_antrian_sekarang)->row();	
			/////

			//DEKLARASI VARIABEL		
			$res_total_antrian_hari_ini=$antrian_sekarang->kunjungan_id;
			$kode_antrian_sekarang=$antrian_sekarang->kunjungan_kode;
			$nama_antrian_sekarang=$id_antrian_sekarang->pendaftaran_nama;
			
		}
		$dokter=$this->Crud->read($dokter)->row();
		$data=array(
			'data'=>$this->Crud->read($antrian_all)->result(),	
			'headline'=>'antrian pasien dokter '.$dokter->dokter_nama,
			'iddokter'=>$id,
			'url'=>$this->default_url,
			'antrian'=>array('res_antrian_sekarang'=>$res_total_antrian_hari_ini,'kode_antrian_sekarang'=>$kode_antrian_sekarang,'id_antrian_sekarang'=>$id_antrian,'nama_antrian_sekarang'=>$nama_antrian_sekarang,),
		);
		$qr_last_antrian="SELECT kunjungan_id FROM kunjunganpasien WHERE kunjungan_tanggal='$date' AND kunjungan_antrian_status=4 AND kunjungan_iddokter=$id";	
		$res_last_antrian=$this->Crud->hardcode($qr_last_antrian)->result();
		if(count($res_last_antrian)<=1){
			$data['last_antrian']=true;
		}else{
			$data['last_antrian']=false;
		}	
		//KEMBALIKAN DATA
		return $data;	
	}
	public function prosesantrian(){
		$id=$this->input->post('id');
		$data=array(
			'kunjungan_antrian_status'=>0,
		);
		$query=array(
			'tabel'=>'kunjunganpasien',
			'data'=>$data,
			'where'=>array('kunjungan_id'=>$id),
		);
		$update=$this->Crud->update($query);
		//AMBIL ID DOKTER
		$qr_id_dokter=array(
			'tabel'=>'kunjunganpasien',
			'where'=>array(array('kunjungan_id'=>$id)),
		);
		$res_id_dokter=$this->Crud->read($qr_id_dokter)->row();
		$id_dokter=$res_id_dokter->kunjungan_iddokter;
		$data=$this->prosesantr($id_dokter);	
		$this->load->view($this->default_view.'antrian',$data);
	
		// $this->viewdata($data);
	}	
	public function antrian($param=null){
		if($param){
			$id=$param;	
		}else{
			$id=$this->input->post('id');
		}
		$data=$this->prosesantr($id);
		$this->load->view($this->default_view.'antrian',$data);
		//$this->viewdata($id);
	}	
	
	public function resetantrian(){
		$id=$this->input->post('id');	
		$kunjungan_antrian_status=4;

		$qr_id_dokter=array(
			'tabel'=>'kunjunganpasien',
			'where'=>array(array('kunjungan_id'=>$id)),
		);
		$res_id_dokter=$this->Crud->read($qr_id_dokter)->row();
		$id_dokter=$res_id_dokter->kunjungan_iddokter;

		$date=date('Y-m-d');
		$query="UPDATE kunjunganpasien SET kunjungan_antrian_status=$kunjungan_antrian_status WHERE kunjungan_iddokter=$id_dokter AND kunjungan_tanggal='$date'";	
		$update=$this->Crud->hardcode($query);
		//AMBIL ID DOKTER
		if($update){
			$data=$this->prosesantr($id_dokter);	
			$this->load->view($this->default_view.'antrian',$data);			
		}else{
			$this->notifiaksi($update);
		}
	}
	public function lewatiantrian(){
		$id=$this->input->post('idlewati');
		$kunjungan_antrian_status=5;
		$qr_id_dokter=array(
			'tabel'=>'kunjunganpasien',
			'where'=>array(array('kunjungan_id'=>$id)),
		);
		$res_id_dokter=$this->Crud->read($qr_id_dokter)->row();
		$id_dokter=$res_id_dokter->kunjungan_iddokter;

		$date=date('Y-m-d');
		$query="UPDATE kunjunganpasien SET kunjungan_antrian_status=$kunjungan_antrian_status WHERE kunjungan_id=$id AND kunjungan_tanggal='$date'";	
		$update=$this->Crud->hardcode($query);
		//AMBIL ID DOKTER
		if($update){
			$data=$this->prosesantr($id_dokter);	
			$this->load->view($this->default_view.'antrian',$data);			
		}else{
			$this->notifiaksi($update);
		}
	}	
	public function previewantrian(){
		$id=$this->input->post('id');
		$date=date('Y-m-d');
		// $query=array(
		// 	'tabel'=>'kunjunganpasien',
		// 	'where'=>array(array('kunjungan_iddokter'=>$id),array('kunjungan_tanggal'=>$date)),
		// 	'order'=>array('kolom'=>'kunjungan_id','orderby'=>'ASC'),
		// );
		$query="SELECT a.*,b.pendaftaran_nama FROM kunjunganpasien a JOIN pendaftaran b ON b.pendaftaran_id=a.kunjungan_idpasien WHERE kunjungan_iddokter=$id AND kunjungan_tanggal='$date'";
		$kunjungan=$this->Crud->hardcode($query)->result();

		////ANTRIAN SEKARNG
		$query_id_antrian_sekarang="SELECT MIN(a.kunjungan_id) as antrian_sekarang, a.kunjungan_kode,b.pendaftaran_nama FROM kunjunganpasien a JOIN pendaftaran b ON b.pendaftaran_id=a.kunjungan_idpasien
			WHERE a.kunjungan_tanggal='$date' AND a.kunjungan_antrian_status=4 AND a.kunjungan_iddokter=$id LIMIT 1";
		$id_antrian_sekarang=$this->Crud->hardcode($query_id_antrian_sekarang)->row();		
		
		$res=array();
		foreach ($kunjungan as $index => $row) {
			$res[$index]=$row;
			if($row->kunjungan_id==$id_antrian_sekarang->antrian_sekarang){
				$res[$index]->antrian_sekarang=true;
			}else{
				$res[$index]->antrian_sekarang=false;
			}
		}
		$data=array(
			'headline'=>'preivew antrian',
			'data'=>$res,
			'url'=>$this->default_url,
		);
		$this->load->view($this->default_view.'previewantrian',$data);	
		// $this->viewdata($data);	
	}

	public function aktivasikehadiran(){
		$id=$this->input->post('id');
		$data=array(
			'kunjungan_antrian_status'=>0,
		);
		$query=array(
			'tabel'=>'kunjunganpasien',
			'data'=>$data,
			'where'=>array('kunjungan_id'=>$id),
		);
		$update=$this->Crud->update($query);
		$this->notifiaksi($update);
		//redirect(site_url($this->default_url));
	}
}
