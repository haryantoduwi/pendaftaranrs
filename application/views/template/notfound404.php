<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Notfound</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
   <link rel="stylesheet" href="<?= base_url();?>asset/fontawesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/AdminLTE.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<div class="wrapper">
  <section class="content">
      <div class="text-center">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Halaman tidak ditemukan.</h3>
        <span class="headline text-yellow" style="font-size: 120px"> 404</span><br>
        <button class="btn btn-danger btn-flat" onclick="history.back(-1)" type="button">Kembali</button>
        <!-- /.error-content -->
      </div> 
  </section>
</div>
</body>
</html>
