<div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Daftar <?php echo ucwords('riwayat tindakan')?></h3>
        </div>
        <div class="box-body table-responsive">
        	<table style="width:100%" id="datatabel" class="table table-bordered table-striped">
                <thead>
	                <tr>
	                  <th width="5%">No</th>
	                  <th width="10%">RM</th>
	                  <th width="15%">Nama</th>
	                  <th width="15%">Telp</th>
	                  <th width="10%">Terdaftar</th>
	                  <th width="35%">alamat</th>
	                  <th width="10%" class="text-center">Aksi</th>
	                </tr>
                </thead>
                <tbody>
                	<?php $i=1;foreach ($data as $row):?>
	                	<tr>
	                		<td><?=$i?></td>
	                		<td><?=$row->pendaftaran_rm?></td>
	                		<td><?=ucwords($row->pendaftaran_nama).' <span class="label '.($row->pendaftaran_status==1 ? 'label-success':'label-danger').'" '.'>'.($row->pendaftaran_status==1 ? 'Aktif':'Belum Aktif').'</span>'?><br>
	                		<td><?=ucwords($row->pendaftaran_notlp)?></td>
	                		<td><?=date('d-m-y',strtotime($row->pendaftaran_terdaftar))?></td>
	                		<td><?=ucwords($row->pendaftaran_alamat)?></td>
	                		<td class="text-center">
	                			<?php include 'button.php';?>
	                		</td>
	                	</tr>	                	
                	<?php $i++;endforeach;?>
                </tbody>            		
        	</table>
        	<p>Keterengan : <br>
        		<a href="#" class="btn btn-flat btn-xs btn-warning" style="width:25px"><span class="fa fa-eye"></span></a> : Detail<br>
        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
        	</p>
        </div>
</div>
<?php include 'action.js'; ?>