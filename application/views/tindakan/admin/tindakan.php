<div class="box box-solid">
	<div class="box-header bg-blue">
		<h3 class="box-title">Pendaftaran</h3>
	</div>	
	<div class="box-body">
		<table class="table table-striped" width="100%">
			<tr>
				<th width="10%">Nama</th>
				<td width="90%">Nama Pasien</td>
			</tr>
		</table>
	</div>
</div>
<div class="box box-solid">
        <div class="box-header bg-green">
          <h3 class="box-title">Daftar <?php echo ucwords('riwayat tindakan')?></h3>
        </div>
        <div class="box-body table-responsive">
        	<table style="width:100%"  class="tindakan table table-bordered table-striped">
                <thead>
	                <tr>
	                  <th width="5%">No</th>
	                  <th width="10%">RM</th>
	                  <th width="15%">Nama</th>
	                  <th width="15%">Telp</th>
	                  <th width="10%">Terdaftar</th>
	                  <th width="35%">alamat</th>
	                  <th width="10%" class="text-center">Aksi</th>
	                </tr>
                </thead>
                <tbody>

                </tbody>            		
        	</table>
        	<p>Keterengan : <br>
        		<a href="#" class="btn btn-flat btn-xs btn-warning" style="width:25px"><span class="fa fa-eye"></span></a> : Detail<br>
        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
        	</p>
        </div>
</div>
<div class="box box-solid">
        <div class="box-header bg-orange">
          <h3 class="box-title">Daftar <?php echo ucwords('riwayat obat')?></h3>
        </div>
        <div class="box-body table-responsive">
        	<table style="width:100%"  class="tindakan table table-bordered table-striped">
                <thead>
	                <tr>
	                  <th width="5%">No</th>
	                  <th width="10%">RM</th>
	                  <th width="15%">Nama</th>
	                  <th width="15%">Telp</th>
	                  <th width="10%">Terdaftar</th>
	                  <th width="35%">alamat</th>
	                  <th width="10%" class="text-center">Aksi</th>
	                </tr>
                </thead>
                <tbody>

                </tbody>            		
        	</table>
        	<p>Keterengan : <br>
        		<a href="#" class="btn btn-flat btn-xs btn-warning" style="width:25px"><span class="fa fa-eye"></span></a> : Detail<br>
        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
        	</p>
        </div>
</div>
<?php include 'action.js'; ?>