<div class="modal-dialog">
<div class="modal-content">
	<div class="modal-header bg-blue">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?=ucwords($headline)?></h4>
		</div>
		<div class="modal-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>Id</th>
						<th>No Pendaftaran</th>
						<th>Nama</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1;foreach($data AS $row):?>
						<tr>
							<td><?= $i ?></td>
							<td><?= $row->kunjungan_id?></td>
							<td><a href="#" url=<?=base_url($url.'aktivasikehadiran')?> class="aktivasikehadiran" onclick="aktivasikehadiran(<?= $row->kunjungan_id?>)"><?= $row->kunjungan_kode?></a></td>
							<td><?= ucwords($row->pendaftaran_nama)?></td>
							<td><span class="label <?= $row->kunjungan_antrian_status=='0' ? 'label-success':($row->kunjungan_antrian_status=='5' ? 'label-danger':'label-warning')?>"><?= $row->kunjungan_antrian_status=='0' ? 'hadir':($row->kunjungan_antrian_status=='5' ? 'tidak hadir':'menunggu')?>
								</span> <i class="text-red"> <?= $row->antrian_sekarang ? ' Sekarang':''?></i>
							</td>
						</tr>					
					<?php $i++;endforeach;?>

				</tbody>
			</table>
			<p class="text-red">Jika ternyata pasien datang, silahkan klik No Pendaftaran maka status tidak hadir akan berubah ke hadir</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger pull-right btn-flat" data-dismiss="modal">Tutup</button>
		</div>
	</div>
</div>
<script type="text/javascript">
	function aktivasikehadiran(id){
	    swal({
	    	title:'Perhatian',
	    	text:'Aktivasi kehadiran ?',
	    	html:true,
	    	ConfirmButtonColor:'#d9534F',
	    	showCancelButton:true,
	    	type:'warning'
	    },function(){
	    	//$("#modalpreviewantrian").modal('hide'); 
	    	var url=$(".aktivasikehadiran").attr('url');
		    $.ajax({
		      type:'POST',
		      url:url,
		      data:{id:id},
		      success:function(data){
		      	$("#modalpreviewantrian").modal('hide'); 
		        //$("#prosesantrian").html(data);       
		      }
		    })
		    
	    });    
	    //alert(url);
	    return false;
	}
</script>