<div class="row"> 
	<?php foreach($data AS $row):?>
		<div class="col-sm-3">
			<div class="box box-solid">
				<div class="box-header bg-blue text-center">
					<h3 class="box-title">Dokter</h3><br>
					<small>Diurutkan berdasarkan urutan tersimpan di database</small>
				</div>
				<div class="box-body table-responsive text-center">   
					<img src="<?= base_url();?>asset/dist/img/user.png" class="img-circle" alt="User Image" style="width: 80px;height: 80px"> 
					<h4><?= ucwords($row->dokter_nama)?></h4>
					<h5>Antrian</h5>
					<p style="font-size: 40px;background-color: #00A65A;color: white">NO RM</p>
				</div>
			</div>		
		</div>
     <?php endforeach;?>
</div>
<?php include 'action.php'; ?>