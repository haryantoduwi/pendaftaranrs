<div id="prosesantrian">
	<div class="box box-solid">
		<div class="box-header bg-blue">
			<h3 class="box-title"><?php echo ucwords($headline)?></h3><br>
			<small>Tanggal : <?= date('d-m-Y')?></small>
		</div>
		<div class="box-body"> 
			<audio id="bell">
				<source src="<?= base_url('./asset/sound/bell.mp3')?>" type="audio/mpeg">
			</audio>				
			<b class="text-red">Total Antrian : <?= count($data)?></b> 	
			<b class="text-red pull-right">Sekarang : <?=  $antrian['res_antrian_sekarang']?></b> 	
			<div class="jumbotron">
				<div class="text-center">
					<b style="font-size:150px" class="counter"><?= $antrian['res_antrian_sekarang']?></b>
					<p style="padding: 10px; background-color:#00A65A;color: white ">No. Antiran : <?=$antrian['kode_antrian_sekarang']?><br>
						Nama : <?= ucwords($antrian['nama_antrian_sekarang'])?>
					</p>
				</div>
			</div>
			<div class="form-group">
				<div class="btn-group pull-left">
					<button type="button" class="btn btn-flat btn-danger" id="tmblresetantrian" url="<?=base_url($url.'resetantrian')?>" onclick="resetantiran(<?=$antrian['id_antrian_sekarang']?>)">Reset</button>
					<button type="button" class="previewantrian btn btn-flat btn-warning" url="<?= base_url($url.'previewantrian')?>" onclick="previewantr(<?=$iddokter?>)"><span class="fa fa-eye" ></span> Preview</button>
					<br>
					<p class="help-block text-red pull-left">Jika dipanggil tidak ada, bisa pilih tombol lewati</p>	
				</div>
				<div class="btn-group pull-right">
					<button type="button" class="btn btn-flat btn-primary" onclick="panggil()"  ><span class="fa fa-bullhorn"></span> Panggil</button>
					<button type="button" id="tmbllewatiantrian" class="btn btn-flat btn-danger <?=$last_antrian ? 'hide':''?>" url="<?=base_url($url.'lewatiantrian')?>" onclick="lewati(<?=$antrian['id_antrian_sekarang']?>)">Lewati</button>
					<a class="btn btn-flat btn-success next_queue <?=$last_antrian ? 'hide':''?>" href="#" role="button" id="tmblantrianselanjutnya" url="<?=base_url($url.'prosesantrian')?>" onclick="prosesantrianselanjutnya(<?=$antrian['id_antrian_sekarang']?>)">
						Selanjutnya &nbsp;<span class="glyphicon glyphicon-chevron-right"></span>
					</a>	        		
				</div>
			</div>
		</div>
	</div>	
</div>
<script type="text/javascript">
   function prosesantrianselanjutnya(id){
    var url=$('#tmblantrianselanjutnya').attr('url');    
    $.ajax({
      type:'POST',
      url:url,
      data:{id:id},
      success:function(data){
        $("#prosesantrian").html(data);       
      }
    })
    //alert(url);
    return false; 
  }
   function resetantiran(id){
    var url=$('#tmblresetantrian').attr('url');  
    swal({
    	title:'Perhatian',
    	text:'Reset Antrian Hari ini',
    	html:true,
    	ConfirmButtonColor:'#d9534F',
    	showCancelButton:true,
    	type:'warning'
    },function(){
	    $.ajax({
	      type:'POST',
	      url:url,
	      data:{id:id},
	      success:function(data){
	        $("#prosesantrian").html(data);       
	      }
	    })
    });
  } 
   function lewati(id){
    var url=$('#tmbllewatiantrian').attr('url');  
    swal({
    	title:'Perhatian',
    	text:'Lewati Antrian Ini ?',
    	html:true,
    	ConfirmButtonColor:'#d9534F',
    	showCancelButton:true,
    	type:'warning'
    },function(){
	    $.ajax({
	      type:'POST',
	      url:url,
	      data:{idlewati:id},
	      success:function(data){
	        $("#prosesantrian").html(data);       
	      }
	    })
    });    
    //alert(url);
    return false; 
  } 
  function previewantr(id){
  	var url=$(".previewantrian").attr('url');
    $.ajax({
      type:'POST',
      url:url,
      data:{id:id},
      success:function(data){
        $("#modalpreviewantrian").html(data);
        $("#modalpreviewantrian").modal('show');       
      }
    })  	
  	//alert(id);
  	return false;
  }   
  function panggil(){
  	//$("#bell") berbentuk objek, harus dikasih 0
  	$("#bell")[0].play();
  } 	
</script>