<div class="row"> 
	<div class="col-sm-6">
		<div class="box box-solid">
	        <div class="box-header bg-blue">
	          <h3 class="box-title">Dokter</h3><br>
	          <small>Antiran diurutkan berdasarkan urutan tersimpan di database</small>
	        </div>
	        <div class="box-body table-responsive">
	        	<div class="col-md-12">
	        		<!-- Widget: user widget style 1 -->
		        	<table style="width:100%" id="datatabel" class="table table-bordered table-striped">
		                <thead >
			                <tr>
			                  <th width="5%">No</th>
			                  <th width="60%">Nama</th>
			                  <th width="10%">Param</th>
			                  <th width="25%">Poli</th>
			                  
			                </tr>
		                </thead>
		                <tbody>
		                	<?php $i=1;foreach ($data as $row):?>
			                	<tr>
			                		<td><?=$i?></td>
			                		<td><a href="#" url="<?= base_url($global->url.'antrian')?>" onclick="antrianpasien(<?= $row->dokter_id ?>)" class="dokter"><?=ucwords($row->dokter_nama)?></a></td>
			                		<td><?=$row->dokter_id?></td>
			                		<td><?=ucwords($row->poli_nama)?></td>                		
			                	</tr>	                	
		                	<?php $i++;endforeach;?>
		                </tbody>            		
		        	</table>
		        </div>       	
	        </div>
		</div>		
	</div>
	<div class="col-sm-6">
		<div id="antrian">
			<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Pilih dokter...</div>
		</div>
		
	</div>
</div>
<?php include 'action.php'; ?>