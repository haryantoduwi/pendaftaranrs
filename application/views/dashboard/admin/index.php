<div id="view">
	<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $jumkunjunganperhari?></h3>

              <p>Kunjungan Pasien</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?=site_url('kunjungan/admin')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $jumpendaftaranperhari?></h3>

              <p>Pendaftaran</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?=site_url('pasienbaru/admin')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>        		
	</div>
	<div class="row">
		<div class="col-sm-12">
          <div class="box box-solid">
            <div class="box-header">
              <h3 class="box-title">Kunjungan Terakhir</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr class="bg-blue">
	                  <th width="5%">No</th>
                      <th width="10%">Kode</th>
                      <th width="10%">Pasien</th>
	                  <th width="10%">Kunjungan</th>
	                  <th width="15%">Poli</th>
                      <th width="15%">Dokter</th>
                      <th width="15%">Tersimpan</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $i=1; 
                  		foreach($kunjunganpasien AS $row):
                  ?>
	                  <tr>
	                		<td><?=$i?></td>
	                		<td><?=ucwords($row->kunjungan_kode)?><br>
                                <?php if($row->kunjungan_status==1):?>
                                    <label class="label label-success">Status : Aktif</label>
                                <?php elseif($row->kunjungan_status==99):?>
                                    <label class="label label-danger">Status : Suspend</label>
                                <?php else:?>
                                    <label class="label label-primary">Status : Menunggu Aktivasi</label>
                                <?php endif;?>    
                            </td>
                            <td><?=ucwords($row->pendaftaran_nama)?></td>
	                		<td><?=date('d-m-Y',strtotime($row->kunjungan_tanggal))?></td>
                            <td><?=ucwords($row->poli_nama)?></td>
                            <td><?=ucwords($row->dokter_nama)?></td>
                            <td><?=date('d-m-Y',strtotime($row->kunjungan_tersimpan))?></td>						
	                  </tr>
                  <?php
                  	$i++;endforeach;
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer clearfix">
              <a href="<?= site_url('kunjungan/admin')?>" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
          </div>
          <!-- /.box -->			
		</div>
	</div>
	<!--
	<div class="row">
		<div class="col-sm-2">
			<div class="form-group">
				<button onclick="add();" id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
		</div>
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
		</div>
	</div>
	-->
</div>
<?php include 'action.js';?>
<script type="text/javascript">
	   //  setTimeout(function () {
    //     var url=$('#tabel').attr('url');
    //     $("#tabel").load(url);
    //     //alert(url);
    // }, 200); 
</script>