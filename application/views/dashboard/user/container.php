<div id="view">
	<?php if($this->session->userdata('user_status')!=1):?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-info"></i> Informasi !</h4>
					Silahkan melakukan aktifasi akun dengan menghubungi bagian <b>Admisi</b>
			</div>			
		</div>
	</div>
	<?php endif;?>
	<div class="row">
		<div class="col-sm-2">
			<div class="form-group">
				<a href="<?= site_url($global->url.'cetakformulir')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-print"></span> Cetak Formulir</a>				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="box box-solid">
			    <div class="box-body box-profile">
			      <p class="bg-blue" style="padding: 5px">Profil</p>

	              <?php if(!$this->session->userdata('user_foto')):?>
			      	<img class="profile-user-img img-responsive img-circle" src="<?= base_url('./asset/dist/img/user.png')?>" alt="User profile picture">
	              <?php else:?>
	                <img src="<?= base_url();?>upload/foto/<?=$this->session->userdata('user_foto')?>" class="profile-user-img img-responsive img-circle" alt="User profile picture">
	              <?php endif;?>
			      <h3 class="profile-username text-center"><?= ucwords($data->pendaftaran_nama)?></h3>

			      <p class="text-muted text-center"><small class="bg-red">No RM</small> <?= $data->pendaftaran_rm?></p>

			      <ul class="list-group list-group-unbordered">
			        <li class="list-group-item">
			          <b>Tempat & Tgl Lahir</b> <br> <a class=""><?= ucwords($data->pendaftaran_tempatlahir).', '.date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?></a>
			        </li>
			        <li class="list-group-item">
			          <b>No.KTP</b><br><a class=""><?= $data->pendaftaran_noktp?></a>
			        </li>
			        <li class="list-group-item">
			          <b>No.Tlp</b><br><a class=""><?= $data->pendaftaran_notlp?></a>
			        </li>
			        <li class="list-group-item">
			          <b>Terdaftar</b><br><a class=""><?= date('d-m-Y',strtotime($data->pendaftaran_terdaftar))?></a>
			        </li>			        
			      </ul>
				  <?php if($this->session->userdata('user_status')!=1):?>
<<<<<<< HEAD
			      <a href=javascript:void()" class="btn btn-flat btn-primary btn-block"><b>Edit Profil</b></a>
			  	<?php else:?>
					<a href="<?= site_url('datadiri/user')?>" class="btn btn-flat btn-primary btn-block disabled"><b>Edit Profil</b></a>
=======
			      <a href=javascript:void()" class="btn btn-flat btn-primary btn-block disabled"><b>Edit Profil</b></a>
			  	<?php else:?>
					<a href="<?= site_url('datadiri/user')?>" class="btn btn-flat btn-primary btn-block "><b>Edit Profil</b></a>
>>>>>>> cb4a1af42ac9d267c03b009607fb60f1a8621f99
			  	<?php endif;?>
			      <p class="help-block text-red <?= $data->pendaftaran_status? 'hide':''?>">Akun anda belum aktif</p>
			    </div>			
			</div>
		</div>
		<div class="col-sm-9">
			<div class="box box-solid">
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped" width="100%">
							<tr>
								<td colspan="2" class="bg-blue" align="left">Data Diri</td>
							</tr>
							<tr>
								<td width="30%" align="left">Alamat</td>
								<td width="70%"><?= ucwords($data->pendaftaran_alamat)?></td>
							</tr>
							<tr>
								<td colspan="2" class="bg-red" align="left">User Sisten</td>
							</tr>
							<tr>
								<td>Username</td>
								<td><?=$data->pendaftaran_username?></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><?=md5($data->pendaftaran_password)?><br><small>*terenkripsi</small></td>
							</tr>														
						</table>
					</div>					
				</div>
			</div>
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Riwayat Kunjungan</h3><br>
              <small>Daftar kunjungan pasien terbaru</small>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
	                <tr>
	                  <th width="5%">No</th>
                      <th width="10%">Kode</th>
                      <th width="10%">Pasien</th>
	                  <th width="10%">Daftar</th>
	                  <th width="15%">Poli</th>
                      <th width="15%">Dokter</th>
                      <th width="15%">Tersimpan</th>
	                </tr>
                  </thead>
                  <tbody>
                	<?php $i=1;foreach ($kunjungan as $row):?>
	                	<tr>
	                		<td><?=$i?></td>
	                		<td><?=ucwords($row->kunjungan_kode)?><br>
                                <?php if($row->kunjungan_status==1):?>
                                    <label class="label label-success">Status : Aktif</label>
                                <?php elseif($row->kunjungan_status==99):?>
                                    <label class="label label-danger">Status : Suspend</label>
                                <?php else:?>
                                    <label class="label label-primary">Status : Menunggu Aktivasi</label>
                                <?php endif;?>    
                            </td>
                            <td><?=ucwords($row->pendaftaran_nama)?></td>
	                		<td><?=date('d-m-Y',strtotime($row->kunjungan_tanggal))?></td>
                            <td><?=ucwords($row->poli_nama)?></td>
                            <td><?=ucwords($row->dokter_nama)?></td>
                            <td><?=date('d-m-Y',strtotime($row->kunjungan_tersimpan))?></td>
	                	</tr>	                	
                	<?php $i++;endforeach;?> 
                  </tbody>
                </table>
              </div>
            </div>
            <div class="box-footer clearfix">
            	<?php if($this->session->userdata('user_status')==1):?>
              		<a href="<?= site_url('riwayat/user')?>" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua</a>
              	<?php else:?>
              		<a href="<?= site_url('riwayat/user')?>" class="btn btn-sm btn-default btn-flat pull-right disabled">Lihat Semua</a>
              	<?php endif;?>
            </div>
          </div>		
		</div>		
		<?php //print_r($data)?>
	</div>
</div>
<?php include 'action.js';?>