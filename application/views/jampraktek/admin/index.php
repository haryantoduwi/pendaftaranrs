<div id="view">
	<div class="row">
		<!--
		<div class="col-sm-2">
			<div class="form-group">
				<button onclick="add();" id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
		</div>
		-->
		<div class="col-sm-2">
			<div class="form-group">
				<button type="button" data-toggle="modal" data-target="#poli" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>			
		</div>
		<div class="col-sm-12">
						
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="poli">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Poli</h4>
      </div>
      <form id="formadd" method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
      <div class="modal-body">
		<div class="form-group">
			<label>Id</label>
			<input type="text" name="id" placeholder="Auto Generated" readonly class="form-control">
		</div>
		<div class="form-group">
			<label>Nama Jam</label>
			<input required type="text" name="jampraktek_nama" class="form-control">
			<p class="help-block">Misal Shift Pagi</p>
		</div>

		<div class="form-group">
			<label>Jam Mulai</label>
			<input type="text" name="jampraktek_mulai" class="timepicker form-control">
		</div>
		<div class="form-group">
			<label>Selesai Praktek</label>
			<input type="text" name="jampraktek_selesai" class="timepicker form-control">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" value="submit" name="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>	
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'action.js';?>
<script type="text/javascript">
	setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 

</script>