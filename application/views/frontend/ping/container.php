<div id="view">

	<div class="row">
		<div class="col-sm-4">
			<div class="form-group">
				<label>Ip Address</label>
				<select class="form-control selectdata" name="perintah" type="tekxt">
					<?php foreach($ip AS $row):?>
						<option value="<?= $row->ip_address?>"><?= $row->ip_alias.' ('.$row->ip_address.')'?> </option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group hide">
				<label>&nbsp</label>
				<button onclick="add();" id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
			<div class="form-group">
				<label>&nbsp</label>
				<button onclick="jalankan();" id="testping" url="<?= base_url($global->url.'testping2')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-chevron-right"></span> Ping</button>
			</div>			
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<!--
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
			-->

			<div class="box box-primary">
				<div class="box-header with-border bg-blue">
					<h3 class="box-title">Hasil Perintah <?= ucwords($global->headline)?></h3>
				</div>
				<div class="box-body">
					<div id="hasil">
						<div class="text-center" style="height: 150px"><i class="fa fa-refresh fa-spin" style="margin-top:50px"></i> Loading data. Please wait...</div>
					</div>						
				</div>
			</div>
		</div>		
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>