<div id="view">
	<script>
 		setTimeout('location.href="<?= site_url('frontend/dashboard')?>"' ,60000);
	</script>
	<div class="row">
		<div class="col-sm-12">
		  <div class="alert alert-info alert-dismissible">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    <h4><i class="icon fa fa-ban"></i>Informasi</h4>
		    	<div id="notifikasi"></div>
		  </div>			
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-4 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-aqua">
		    <div class="inner">
		      <h3><?=count($ipaddress)?></h3>
		      <p>Devices</p>
		    </div>
		    <div class="icon">
		      <i class="fa fa-desktop"></i>
		    </div>
		    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div>			
		<div class="col-lg-4 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-green">
		    <div class="inner">
		      <h3><?=$widget['hidup']?></h3>
		      <p>UP</p>
		    </div>
		    <div class="icon">
		      <i class="fa  fa-arrow-up"></i>
		    </div>
		    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div>
		<div class="col-lg-4 col-xs-6">
		  <!-- small box -->
		  <div class="small-box bg-red">
		    <div class="inner">
		      <h3><?=$widget['mati']?></h3>
		      <p>Down</p>
		    </div>
		    <div class="icon">
		      <i class="fa  fa-arrow-down"></i>
		    </div>
		    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		  </div>
		</div>					
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-solid">
				<div class="box-header with-border bg-grey">
					<h3 class="box-title">IP Address</h3>
				</div>
				<div class="box-body table-responsive">
					<table width="100%" class="table table-striped" id="datatabel2">
						<thead>
						<tr class="bg-blue">
							<td width="5%">No</td>
							<td width="30%">Alias</td>
							<td width="50%">Ip Address</td>
							<td width="15%" class="text-center">Status</td>
						</tr>
						</thead>
						<tbody>
						<?php $i=1;foreach($ipaddress AS $row):?>
							<tr class="<?= $row->hidup==1 ? '':'bg-red'?>">
								<td><?= $i?></td>
								<td><?= $row->ip_alias?></td>
								<td><?= $row->ip_address?></td>
								<td class="text-center"><span class="label <?= $row->hidup==1? 'label-success':'label-danger'?>">
									<?= $row->hidup==1? '<span class="fa fa-arrow-up"></span> Hidup':'<span class="fa fa-arrow-down"></span> Down'?>
								</span></td>
							</tr>
						<?php $i++;endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>
<script>
    var url = "<?= site_url('frontend/dashboard')?>"; // url tujuan
    var count = 60; // dalam detik
    function countDown() {
        if (count > 0) {
            count--;
            var waktu = count + 1;
            $('#notifikasi').html('Halaman akan refresh dalam ' + waktu + ' detik.');
            setTimeout("countDown()", 1000);
        } else {
            window.location.href = url;
        }
    }
    countDown();
</script>