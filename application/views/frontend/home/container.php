<div id="view">
	<div class="row">
        <div class="col-md-9">
        	<div class="form-group">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
					  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
					  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
					</ol>
					<div class="carousel-inner">
					  <div class="item active">
					    <img src="http://placehold.it/900x400/39CCCC/ffffff&text=I+Love+Bootstrap" alt="First slide">

					    <div class="carousel-caption">
					      First Slide
					    </div>
					  </div>
					  <div class="item">
					    <img src="http://placehold.it/900x400/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">

					    <div class="carousel-caption">
					      Second Slide
					    </div>
					  </div>
					  <div class="item">
					    <img src="http://placehold.it/900x400/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">

					    <div class="carousel-caption">
					      Third Slide
					    </div>
					  </div>
					</div>
					<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
					  <span class="fa fa-angle-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
					  <span class="fa fa-angle-right"></span>
					</a>
				</div>
			</div>
        </div>
        <div class="col-sm-3">
        	<div class="box box-solid">
        		<div class="box-header bg-blue">
        			<h3 class="box-title"> <span class="fa fa-lock"></span> Login User</h3>
        		</div>
        		<div class="box-body">
        			<form id="formadd" method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
        				<div class="form-group">
        					<label>Username</label>
        					<input required type="text" name="user_username" class="form-control">
        				</div>
        				<div class="form-group">
        					<label>Password</label>
        					<input required type="password" name="user_password" class="form-control">
        				</div>
        				<div class="form-group">
        					<button type="submit" class="btn btn-block btn-flat btn-primary"><span class="fa fa-paper-plane"></span> Login</button><br>
        					<a href="<?= site_url('frontend/pendaftaran')?>">Belum punya akun ?</a>
        				</div>
        				<div class="form-group">
        					<a href="<?= site_url('frontend/pendaftaranpoli')?>" class="btn btn-block btn-flat btn-success"><span class="fa fa-bookmark-o"></span> Daftar Poli</a>
        				</div        				        				        				
        			</form>
        		</div>
        	</div>
        </div>		
	</div>
</div>
<?php include 'action.php';?>