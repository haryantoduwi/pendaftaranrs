<div class="row">
	<div class="col-sm-12">
		<div class="box box-solid">
			<div class="box-header with-border bg-blue">
				<h3 class="box-title">Form Pendaftaran Pasien</h3>
			</div>
			<form id="formadd" method="POST" action="javascript:void(0)" url="<?= base_url($global->url)?>"  enctype="multipart/form-data">
			<div class="box-body">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group bg-blue" style="padding: 5px">
							<b> <span class="fa fa-users"></span> Data Diri</b>
						</div>
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input required  type="text" name="pendaftaran_nama" class="form-control">
						</div>
						<div class="form-group">
							<label>Tempat Lahir</label>
							<input type="text" name="pendaftaran_tempatlahir" class="form-control">
						</div>
						<div class="form-group">
							<label>No.Telp</label>
							<input type="text" name="pendaftaran_notlp" class="form-control">
						</div>												
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input type="text" name="pendaftaran_tgllahir" class="form-control datepicker">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
								<label>
									<input type="radio" name="pendaftaran_jeniskelamin" id="optionsRadios1" value="1" checked>
									 Laki-Laki
								</label>
								<label style="margin-left: 10px">
									<input type="radio" name="pendaftaran_jeniskelamin" id="optionsRadios2" value="0">
										Perempuan
								</label>								
							</div>
						</div>														
						<div class="form-group">
							<label>No KTP</label>
							<input required type="text" name="pendaftaran_noktp" class="form-control">
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea required type="text" name="pendaftaran_alamat" class="form-control"></textarea>
						</div>																	
					</div>
					<div class="col-sm-6">
						<div class="form-group bg-green" style="padding: 5px">
							<b> <span class="fa fa-users"></span> Username Sistem</b>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input required  type="text" name="pendaftaran_username" class="form-control">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input required  type="password" name="pendaftaran_password" id="password" class="form-control">
						</div>
						<div class="form-group">
							<label>Ketik Ulang Password</label>
							<input required  type="password" equalto="#password" class="form-control">
						</div>																								
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<button type="submit" value="submit" name="submit" class="btn bg-blue btn-flat btn-block">Daftar</button>
						</div> 
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>	
<?php include 'action.php';?>