<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= ucwords($judul)?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url();?>asset/fontawesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> 
</head>
<style type="text/css">
/*  @media print {
      #Header, #Footer { display: none !important; }
  }  */
</style>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-newpaper-o"></i> Pendaftaran Poli Mandiri
          <small class="pull-right">Tanggal: <?= date('d-m-Y',strtotime($data->kunjungan_tersimpan))?></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped" width="100%">          
          <tr>
            <td colspan="3" >
              <table width="100%">
                <td width="40%" align="right">
                  <img src="<?= base_url('./asset/dist/img/arraymotion.png')?>" width="120px" height="120px">
                </td>
                <td width="60%" abbr="left">
                  <h3>Nama Rumahsakit</h3>
                  <h4>Alamat di jalan sebrang pulau</h4>
                </td>               
              </table>
            </td>
          </tr>
          <tr>
            <th width="30%">No RM</th>
            <td width="70%"><?=ucwords($data->pendaftaran_rm)?> </td>
            <td rowspan="6" width="20%">
              <?php if($data->pendaftaran_foto):?>
              <img src="<?= base_url('./upload/foto/'.$data->pendaftaran_foto)?>" class="img img-circle"  width="120px" height="120px">
              <?php else:?>
                <img src="<?= base_url();?>asset/dist/img/user.png" class="img img-circle"  width="120px" height="120px">
              <?php endif;?>
            </td>
          </tr>
          <tr>
            <th>Nama</th>
            <td><?=ucwords($data->pendaftaran_nama)?></td>
          </tr> 
          <tr>
            <th>Nomor Pendaftaran</th>
            <td><?= $data->kunjungan_kode?></td>
          </tr>
          <tr>
            <th>Jam</th>
            <td><?= $data->jampraktek_mulai.' - '.$data->jampraktek_selesai.' / '.$data->jampraktek_nama?></td>
          </tr>         
          <tr>
            <th>Tanggal</th>
            <td><?= date('d-m-Y',strtotime($data->kunjungan_tanggal))?></td>
          </tr>
          <tr>
            <th>Poli</th>
            <td><?= ucwords($data->poli_nama)?></td>
          </tr>
          <tr>
            <th>Dokter</th>
            <td><?= ucwords($data->dokter_nama)?></td>
          </tr>                                                
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


  </section>
</div>
</body>
</html>
