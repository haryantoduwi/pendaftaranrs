<?php if(!$data):?>
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Perhatian !</h4>
    	Jadwal tidak ditemukan
  </div>
<?php else:?>
	<!--
	<div class="form-group">
		<label>Status Pasien</label>
		<select class="form-control selectdata" style="width: 100%" name="daftar_status">
			<option value=" bpjs">BPJS</option>
			<option value="umum">UMUM</option>
		</select>
	</div>
	-->									
	<div  class="form-group">
		<label>Jam Praktek</label>
		<div class="input-group ">
			<select required class="form-control selectdata" style="width: 100%" onchange="getkuota()" id="jampraktek" name="daftar_idjam">
				<option disabled="disabled" selected="selected" value="">Pilih Jam</option>
				<?php foreach($data AS $row):?>
					<option value="<?= $row->jampraktek_id?>"><?= $row->jampraktek_nama.' / '.$row->jampraktek_mulai.' - '.$row->jampraktek_selesai?></option>
				<?php endforeach;?>
			</select>	
			<div class="input-group-btn">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdl-jadwaldokter"> <span class="fa fa-question"></span> Jadwal</button>
			</div>	
		</div>
		<div class="help-block with-errors"></div>			 
	</div>
	<div class="form-group">
		<label>Tanggal</label>
		<input required type="text" name="kunjungan_tanggal" class="form-control datepicker" id="pilihtanggal">
		<div class="help-block with-errors"></div>
	</div>
	<div class="form-group">
		<label>Dokter</label>
			<select required class="form-control selectdata" style="width: 100%" onchange="getkuota()"  name="kunjungan_iddokter">
				<option disabled="disabled" selected="selected" value="">Pilih Dokter</option>
				<?php foreach($jadwal AS $row):?>
					<option value="<?= $row->dokter_id?>"><?= ucwords($row->dokter_nama)?></option>
				<?php endforeach;?>
			</select>	
			<div class="help-block with-errors"></div>		
	</div>		
	<div class="form-group">
		<div id="kuota"></div>
	</div>	
	<!--MODAL JADWAL DOKER-->
	<div class="modal fade" id="mdl-jadwaldokter">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Jadwal Dokter Poli <?= ucwords($poli->poli_nama)?></h4>
	      </div>
	      <div class="modal-body">
	       	<div class="table-responsive">
	       		<table width="100%" class="table table-hover">
	       			<tr>
	       				<td colspan="3" class="bg-blue">Tanggal <?= date('d-m-Y')?></td>
	       			</tr>
	       			<tr>
	       				<td width="5%">No</td>
	       				<td width="65%">Nama</td>
	       				<td width="30%">Jam</td>
	       			</tr>
	       			<?php $i=1; foreach($jadwal AS $row):?>
	       				<tr>
	       					<td><?=$i?></td>
	       					<td><?= ucwords($row->dokter_nama)?></td>
	       					<td><?= $row->jampraktek_mulai.' sampai '.$row->jampraktek_selesai?></td>
	       				</tr>
	       			<?php $i++;endforeach;?>
	       		</table>
	       	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	       
	      </div>
	    </div>
	  </div>
	</div>		
<?php endif;?>

<script type="text/javascript">
  $(document).ready(function(){
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: true,
    });
    $(".selectdata").select2();             
  })	
	function getkuota(){
	    idjampraktek=$('#jampraktek').val();
	 	tanggal=$('#pilihtanggal').val();
	    pilihpoli=$('#pilihpoli').val();
	    url="<?= base_url('frontend/pendaftaranpoli/kuota');?>";
	    //alert(url);
	    $.ajax({
	      type:'POST',
	      url:url,
	      data:{idjampraktek:idjampraktek,idpoli:pilihpoli,pilihtanggal:tanggal},
	      success:function(data){
	        $("#kuota").html(data);       
	      }
	    })   
	    return false; 
	}  
</script>