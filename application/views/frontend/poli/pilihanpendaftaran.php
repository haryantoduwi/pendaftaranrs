<?php if($pilihan=='poli'):?>
<div class="form-group" id="formpilihpoli">
	<label>Poli</label>
	<select required class="form-control selectdata" id="pilihpoli" onchange="getjadwal()" style="width: 100%" name="daftar_idpoli" >
		<option disabled="disabled" selected="selected" value="">Pilih Poli</option>
		<?php foreach($poli AS $row):?>
			<option value="<?= $row->poli_id?>"><?= $row->poli_nama?></option>
		<?php endforeach;?>
	</select>
	<div class="help-block with-errors"></div>
</div>
<?php else:?>
<div class="form-group" id="formpilihdokter">
    <label>Dokter</label>
    <select required class="form-control selectdata" id="pilihdokter" onchange="getatributdokter()" style="width: 100%" name="kunjungan_iddokter" >
        <option disabled="disabled" selected="selected" value="">Pilih Dokter</option>
        <?php foreach($dokter AS $row):?>
            <option value="<?= $row->dokter_id?>"><?= ucwords($row->dokter_nama)?></option>
        <?php endforeach;?>
    </select>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <label>Tanggal</label>
    <input type="text" name="kunjungan_tanggal" class="form-control datepicker" >
</div>      
<?php endif;?>    

<div id="tampiljadwal"></div>
<div id="tampilatributdokter"></div>

<script type="text/javascript">
  $(document).ready(function(){
    
    $('#datatabel').DataTable();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: true,
    });
    $(".selectdata").select2();             
  })   
</script>