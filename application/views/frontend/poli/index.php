<?php if($data):?>
	<?php require_once('formulirpendaftaran.php');?>
<?php else:?>
	<div id="view">
		<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
			<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
		</div>
	</div>
<?php endif;?>

<script type="text/javascript">
	$(document).ready(function(){
		var url=$('#tabel').attr('url');
		    setTimeout(function () {
	        $("#tabel").load(url);
	        //alert(url);
	    }, 200); 		
	})	
</script>
<?php include 'action.php';?>