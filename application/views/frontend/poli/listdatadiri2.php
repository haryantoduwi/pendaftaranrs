<div class="box box-solid">
	<div class="box-body">
		<?php if(!$data):?>
		  <div class="alert alert-danger alert-dismissible">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    <h4><i class="icon fa fa-check"></i> Perhatian !</h4>
		    	Nomor RM tidak ditemukan
		  </div>
		<?php else:?>
		        <form action="<?= base_url($global->url)?>" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
		        <div id="smartwizard">
		            <ul>
		                <li><a href="#step-1">Step 1<br /><small>Data Diri</small></a></li>
		                <li><a href="#step-2">Step 2<br /><small>Pendaftaran</small></a></li>
		                <li><a href="#step-3">Step 3<br /><small>Persetujuan</small></a></li>
		            </ul>

		            <div>
		                <div id="step-1">
		                    <h2>Data Diri</h2>
		                    <p class="text-red"><i>Pastikan data diri anda sudah benar</i></p>
		                    <div id="form-step-0" role="form" data-toggle="validator">
								<div class="form-group hide">
									<label>Param</label>
									<input required readonly type="text" name="kunjungan_idpasien" class="form-control" value="<?= $data->pendaftaran_id?>">
								</div>			                    	
								<div class="form-group">
									<label>No RM</label>
									<input required readonly type="text" name="pendaftaran_noktp" class="form-control" value="<?= $data->pendaftaran_rm?>">
								</div>						
								<div class="form-group">
									<label>Nama Lengkap</label>
									<input required  readonly type="text" name="pendaftaran_nama" class="form-control text-capitalize" value="<?= $data->pendaftaran_nama?>">
								</div>
								<div class="form-group">
									<label>Tempat Lahir</label>
									<input type="text" readonly name="pendaftaran_tempatlahirx" class="form-control" value="<?= ucwords($data->pendaftaran_tempatlahir).', '.date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?>">
								</div>		                        
		                    </div>
		                </div>
		                <div id="step-2">
		                    <h2>Pendaftaran</h2>
		                    <div id="form-step-1" role="form" data-toggle="validator">
                                <div class="form-group">
                                    <label>Pilih Jeni Pendaftaran</label>
                                    <div class="radio" >
                                        <label style="margin-right: 30px">
                                            <input type="radio" required name="optionsRadios" id="optionsRadios1" value="poli" class="jenispendaftaran" onclick="jenispendaftaran('poli')">
                                            Poli
                                        </label>
                                        <label>
                                            <input type="radio" required name="optionsRadios" id="optionsRadios2" value="dokter" class="jenispendaftaran" onclick="jenispendaftaran('dokter')">
                                            Dokter
                                        </label>                                        
                                    </div>
                                </div>
                                <div id="loadpilihan">
                                </div>									
		                    </div>
		                </div>
		                <div id="step-3" class="">
		                    <h2>Terms and Conditions</h2>
		                    <p>
		                        Terms and conditions: Keep your smile :)
		                    </p>
		                    <div id="form-step-3" role="form" data-toggle="validator">
		                        <div class="form-group">
		                            <label for="terms">Saya setuju dengan peraturan yang telah ditetapkan</label>
		                            <input type="checkbox" id="terms" data-error="Please accept the Terms and Conditions" required>
		                            <div class="help-block with-errors"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		        </form>
		<?php endif;?>			
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
        .addClass('btn btn-info')
        .on('click', function(){
        	if( !$(this).hasClass('disabled')){
        		var elmForm = $("#myForm");
        		if(elmForm){
        			elmForm.validator('validate');
        			var elmErr = elmForm.find('.has-error');
        			if(elmErr && elmErr.length > 0){
        				alert('Maaf, silahkan isi semua form');
        				return false;
        			}else{
        				alert('Simpan berhasil');
        				elmForm.submit();
        				return false;
        			}
        		}
        	}
        });
        var btnCancel = $('<button></button>').text('Cancel')
        .addClass('btn btn-danger')
        .on('click', function(){
        	$('#smartwizard').smartWizard("reset");
        	$('#myForm').find("input, textarea").val("");
        });
        // Smart Wizard
        $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                transitionEffect:'fade',
                // toolbarSettings: {toolbarPosition: 'bottom',
                //                   toolbarExtraButtons: [btnFinish, btnCancel]
                //                 },
                toolbarSettings: {toolbarPosition: 'bottom',
                                  toolbarExtraButtons: [btnFinish]
                                },                
                anchorSettings: {
                            markDoneStep: true, // add done css
                            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                        }
             });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#form-step-" + stepNumber);
            // stepDirection === 'forward' :- this condition allows to do the form validation
            // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            if(stepDirection === 'forward' && elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.children('.has-error');
                if(elmErr && elmErr.length > 0){
                    // Form validation failed
                    return false;
                }
            }
            return true;
        });

        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            // Enable finish button only on last step
            if(stepNumber == 3){
                $('.btn-finish').removeClass('disabled');
            }else{
                $('.btn-finish').addClass('disabled');
            }
        });

    });
</script>
<script type="text/javascript">
  $(document).ready(function(){
  	
    $('#datatabel').DataTable();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: true,
    });
    $(".selectdata").select2();             
  })
  function getjadwal(){
    idpoli=$('#pilihpoli').val();
    url="<?= base_url('frontend/pendaftaranpoli/getjadwal');?>";
    //alert(url);
    $.ajax({
      type:'POST',
      url:url,
      data:{idpoli:idpoli},
      success:function(data){
        $("#tampiljadwal").html(data);       
      }
    })   
    return false; 
  }
  function getatributdokter(){
    iddokter=$('#pilihdokter').val();
    url="<?= base_url('frontend/pendaftaranpoli/getatributdokter');?>";
    //alert(iddokter);
    $.ajax({
      type:'POST',
      url:url,
      data:{iddokter:iddokter},
      success:function(data){
        $("#tampilatributdokter").html(data);       
      }
    })   
    return false; 
  }  
  function jenispendaftaran(data){
    url="<?= base_url('frontend/pendaftaranpoli/jenispendaftaran');?>";
    //alert(url);
    $.ajax({
      type:'POST',
      url:url,
      data:{pilihan:data},
      success:function(data){
        $("#loadpilihan").html(data);       
      }
    })   
    return false; 
  }    
</script>