<table class="table table-striped" width="100%">					
	<tr>
		<td colspan="2" >
			<table width="100%">
				<td width="40%" align="right">
					<img src="<?= base_url('./asset/dist/img/arraymotion.png')?>" width="120px" height="120px">
				</td>
				<td width="60%" abbr="left">
					<h3>Nama Rumahsakit</h3>
					<h4>Alamat di jalan sebrang pulau</h4>
				</td>								
			</table>
		</td>
	</tr>
	<tr>
		<th width="20%">No RM</th>
		<td width="80%"><?=ucwords($data->pendaftaran_rm)?> </td>
	</tr>
	<tr>
		<th width="20%">Nama</th>
		<td width="80%"><?=ucwords($data->pendaftaran_nama)?></td>
	</tr>	
	<tr>
		<th>Nomor Pendaftaran</th>
		<td><?= $data->kunjungan_kode?></td>
	</tr>
	<tr>
		<th>Jam</th>
		<td><?= $data->jampraktek_mulai.' - '.$data->jampraktek_selesai.' / '.$data->jampraktek_nama?></td>
	</tr>					
	<tr>
		<th>Tanggal</th>
		<td><?= date('d-m-Y',strtotime($data->kunjungan_tanggal))?></td>
	</tr>
	<tr>
		<th>Poli</th>
		<td><?= ucwords($data->poli_nama)?></td>
	</tr>
	<tr>
		<th>Dokter</th>
		<td><?= ucwords($data->dokter_nama)?></td>
	</tr>
	<tr>
		<th colspan="2">
			<a href="#" class="btn btn-flat btn-primary pull-right"><i class="fa fa-print"></i> Cetak</a>
		</th>
		
	</tr>																									
</table>