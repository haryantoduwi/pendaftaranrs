<div class="box box-solid">
	<div class="box-body">
		<?php if(!$data):?>
		  <div class="alert alert-danger alert-dismissible">
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		    <h4><i class="icon fa fa-check"></i> Perhatian !</h4>
		    	Nomor RM tidak ditemukan
		  </div>
		<?php else:?>
			<form action="<?= base_url($global->url)?>" method="POST"> 				
				<div class="row">
					<div class="col-sm-6">						
						<div class="form-group bg-blue" style="padding: 5px">
							<b> <span class="fa fa-users"></span> Data Diri</b>
						</div>
						<p class="text-red"><i>Pastikan data diri anda sudah benar</i></p>
						<div class="form-group">
							<label>Param</label>
							<input required readonly type="text" name="kunjungan_idpasien" class="form-control" value="<?= $data->pendaftaran_id?>">
						</div>						
						<div class="form-group">
							<label>No RM</label>
							<input required readonly type="text" name="pendaftaran_norm" class="form-control" value="<?= $data->pendaftaran_rm?>">
						</div>							
						<div class="form-group">
							<label>No KTP</label>
							<input required readonly type="text" name="pendaftaran_noktp" class="form-control" value="<?= $data->pendaftaran_noktp?>">
						</div>						
						<div class="form-group">
							<label>Nama Lengkap</label>
							<input required  readonly type="text" name="pendaftaran_nama" class="form-control text-capitalize" value="<?= $data->pendaftaran_nama?>">
						</div>
						<div class="form-group">
							<label>Tempat Lahir</label>
							<input type="text" readonly name="pendaftaran_tempatlahirx" class="form-control" value="<?= ucwords($data->pendaftaran_tempatlahir).', '.date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?>">
						</div>
						<div class="form-group">
							<label>No.Telp</label>
							<input type="text" readonly name="pendaftaran_notlp" class="form-control" value="<?=$data->pendaftaran_notlp?>">
						</div>												

						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="radio">
								<label>
									<input type="radio" name="pendaftaran_jeniskelamin" id="optionsRadios1" value="1" <?= $data->pendaftaran_jeniskelamin==1 ? 'checked':''?>>
									 Laki-Laki
								</label>
								<label style="margin-left: 10px">
									<input type="radio" name="pendaftaran_jeniskelamin" id="optionsRadios2" value="0" <?= $data->pendaftaran_jeniskelamin==0 ? 'checked':''?>>
										Perempuan
								</label>								
							</div>
						</div>
						<div class="form-group">
							<label>Alamat</label>
							<textarea required readonly type="text" name="pendaftaran_alamat" class="form-control"><?= ucwords($data->pendaftaran_alamat)?></textarea>
						</div>							
					</div>
					<div class="col-sm-6">
						<div class="form-group bg-blue" style="padding: 5px">
							<b> <span class="fa fa-users"></span> Poli</b>
						</div>				
						<div class="form-group">
							<label>Poli</label>
							<select class="form-control selectdata" id="pilihpoli" onchange="getjadwal()" style="width: 100%" name="daftar_idpoli" >
								<option disabled="disabled" selected="selected">Pilih Poli</option>
								<?php foreach($poli AS $row):?>
									<option value="<?= $row->poli_id?>"><?= $row->poli_nama?></option>
								<?php endforeach;?>
							</select>
						</div>
						<div id="tampiljadwal"></div>																		
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">

					</div>
				</div>	
			</form>	
		<?php endif;?>			
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
  	
    $('#datatabel').DataTable();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: true,
    });
    $(".selectdata").select2();             
  })
  function getjadwal(){
    idpoli=$('#pilihpoli').val();
    url="<?= base_url('frontend/pendaftaranpoli/getjadwal');?>";
    //alert(url);
    $.ajax({
      type:'POST',
      url:url,
      data:{idpoli:idpoli},
      success:function(data){
        $("#tampiljadwal").html(data);       
      }
    })   
    return false; 
  }  
</script>