<div class="row">
	<div class="col-sm-12">
		<div class="box box-solid">
			<div class="box-header with-border bg-blue">
				<h3 class="box-title">Pendaftaran Mandiri Poli</h3><br>
				<small>Silahkan mengisi form pendaftaran online</small>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label>No RM</label>
							<input required type="text" id="norm" name="norm" class="form-control" list="norm" value="RM190002">
							<p class="help-block">Masukkan no rm</p>
							<!--
							<datalist id="norm">
								<?php foreach($data AS $row):?>
									<option value="<?= $row->pendaftaran_rm?>">
								<?php endforeach;?>
							</datalist>
							-->							
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label>Tgl Lahir</label>
							<input type="text" required id="tgllahir" name="tgllahir" class="form-control datemask" >						
						</div>
					</div>					
					<div class="col-sm-2">
						<div class="form-group">	
							<label>&nbsp</label>
							<button type="button" class="btn btn-block btn-flat btn-primary" id="ceknorm" url="<?= base_url($global->url.'ceknorm')?>" onclick="cekrm()"><span class="fa  fa-arrow-right"></span> Lanjut</button>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div id="listdatadiri"></div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".datemask").inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
	})
</script>