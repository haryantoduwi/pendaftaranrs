<div id="antrian" url="<?= base_url($global->url.'tabel')?>">
	<div class="row"> 
		<?php foreach($data AS $row):?>
			<div class="col-sm-3">
				<div class="box box-solid">

					<div class="box-body text-center">   
						<img src="<?= base_url();?>asset/dist/img/user.png" class="img-circle" alt="User Image" style="width: 80px;height: 80px"> 
						<h5>
							<?= ucwords(character_limiter($row->dokter_nama,15))?>
						</h5>
					
						<h5>Antrian</h5>
						<p style="font-size: 24px;background-color: #00A65A;color: white"><?= $row->antrian_sekarang?></p>
						<small><?= ucwords($row->antrian_nama)?></small>
					</div>
				</div>		
			</div>
	     <?php  endforeach; ?>
	</div>	
</div>