<div class="row">
	<div class="col-sm-12 animated bounceInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form id="formadd" method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Id</label>
								<input type="text" name="id" placeholder="Auto Generated" readonly class="form-control">
							</div>
							<div class="form-group">
								<label>NIK Dokter</label>
								<input required type="text" name="dokter_nik" class="form-control">
							</div>							
							<div class="form-group">
								<label>Nama</label>
								<input required type="text" name="dokter_nama" class="form-control">
							</div>
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<div class="radio">
									<label>
										<input type="radio" name="dokter_jeniskelamin"  value="1" checked>
										Laki-laki
									</label>
									<label style="margin-left: 30px">
										<input type="radio" name="dokter_jeniskelamin"  value="2">
										Perempuan
									</label>									
								</div>
							</div>							
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="text" name="dokter_tgllahir" class="datepicker form-control">
							</div>
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input required type="text" name="dokter_tempatlahir" class="form-control">
							</div>							
							<div class="form-group">
								<label>Nomor Telp.</label>
								<input type="text" name="dokter_notlp" class="form-control">
							</div>	
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" rows="3" name="dokter_alamat"></textarea>
							</div>																																											 
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>Jam Praktek</label>
								<select type="text" name="dokter_idjampraktek" class="form-control selectdata" style="width: 100%">
									<?php foreach($jampraktek AS $row):?>
										<option value="<?= $row->jampraktek_id?>"><?= ucwords($row->jampraktek_nama).'/'.$row->jampraktek_mulai.'-'.$row->jampraktek_selesai?></option>
									<?php endforeach;?>
								</select>
							</div>		
							<div class="form-group">
								<label>Poli</label>
								<select type="text" name="dokter_poli" class="form-control selectdata" style="width: 100%">
									<?php foreach($poli AS $row):?>
										<option value="<?= $row->poli_id?>"><?= $row->poli_nama?></option>
									<?php endforeach;?>
								</select>
							</div>							
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Foto</label>
								<input type="file" name="fileupload">
								<p class="help-block">Ukuran maksimal 5mb, jpg atau png</p>
							</div>							
							<div class="form-group">
								<button type="submit" value="submit" name="submit" class="btn btn-primary btn-block btn-flat">Simpan</button>
							</div>														
						</div>
					</div>
				</form>		
			</div>
		</div>		
	</div>	
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
</script>
<?php include 'action.js'?>