<div id="view">
	<div class="row">
		<div class="col-sm-12">
			<h4>Tanggal : <?= date('d-m-Y')?></h4>
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
		</div>
	</div>
</div>
<?php include 'action.php';?>
<script type="text/javascript">
	setInterval(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 1000);	    
</script>