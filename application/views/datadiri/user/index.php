<div id="view">
	<div class="row">
		<div class="col-sm-12">
			<form action="<?= base_url($global->url)?>" method="POST" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-offset-10 col-sm-2">
					<div class="form-group">
						<button type="submit" name="submit" value="submit" class="btn btn-flat btn-primary btn-block">Update</button>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Data Diri</h3><br>
							<small>Data yang diisikan adalah data terbaru, update data jika ada perubahan data diri anda</small>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>No RM</label>
								<input type="text" disabled name="pendaftaran_rm" class="form-control" value="<?= $data->pendaftaran_rm?>">
							</div>	
							<div class="form-group hide">
								<label>Param</label>
								<input type="text" readonly name="id" class="form-control " value="<?= $data->pendaftaran_id?>">
							</div>											
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="pendaftaran_nama" class="form-control" value="<?= ucwords($data->pendaftaran_nama)?>">
							</div>
							<div class="form-group">
								<label>Jenis Kelamin</label>
								<div class="radio">
									<label>
										<input type="radio" name="pendaftaran_jeniskelamin" id="jeniskelamin" value="1" <?= $data->pendaftaran_jeniskelamin==1 ? 'checked':''?>>
										Laki-Laki
									</label>
									<label style="margin-left: 10px">
										<input type="radio" name="pendaftaran_jeniskelamin" id="jeniskelamin" value="0" <?= $data->pendaftaran_jeniskelamin==0 ? 'checked':''?>>
										Perempuan
									</label>							
								</div>
							</div>
							<div class="form-group">
								<label>Tempat Lahir</label>
								<input type="text" name="pendaftaran_tempatlahir" class="form-control" value="<?= $data->pendaftaran_tempatlahir?>">
							</div>
							<div class="form-group">
								<label>Tanggal Lahir</label>
								<input type="text" name="pendaftaran_tgllahir" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?>">
							</div>	
							<div class="form-group">
								<label>No KTP/KK</label>
								<input type="text" name="pendaftaran_noktp" class="form-control" value="<?= $data->pendaftaran_noktp?>">
							</div>
							<div class="form-group">
								<label>No Telp</label>
								<input type="text" name="pendaftaran_notlp" class="form-control" value="<?= $data->pendaftaran_notlp?>">
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" type="text" name="pendaftaran_alamat" rows="4"><?= ucwords($data->pendaftaran_alamat)?></textarea>
							</div>												
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">User Sistem</h3><br>
							<small>Update data user sistem secara berkala untuk keamanan akun</small>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Username</label>
								<input type="text" name="pendaftaran_username" class="form-control" value="<?= $data->pendaftaran_username ?>">
							</div>					
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="pendaftaran_password" class="form-control">
								<p class="help-block text-red">Kosongkan jika tidak dirubah</p>
							</div>							
						</div>
					</div>
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Berkas</h3><br>
							<small>Berkas yang diupload maksimal berukuran 3 Mb dengan format jpg,jpeg</small>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Foto</label>
								<input type="file"  name="pendaftaran_foto">
								<label class="label label-info">File Tersimpan : <?= $data->pendaftaran_foto?></label>
							</div>					
							<div class="form-group">
								<label>KTP</label>
								<input type="file" name="pendaftaran_ktp">
								<label class="label label-info">File Tersimpan : <?= $data->pendaftaran_ktp?></label>
							</div>							
						</div>
					</div>
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Data Wali</h3><br>
							<small>Isikan data wali yang dikehendaki</small>
						</div>
						<div class="box-body">
							<div class="form-group">
								<label>Nama</label>
								<input type="text"  name="pendaftaran_namawali" class="form-control" value="<?= $data->pendaftaran_namawali ?>">
							</div>					
							<div class="form-group">
								<label>Alamat</label>
								<textarea class="form-control" type="text" name="pendaftaran_alamatwali" rows="4"><?= $data->pendaftaran_alamatwali ?></textarea>
							</div>							
						</div>
					</div>						
				</div>
			</div>		
			</form>
		</div>
	</div>

	<!--
	<div class="row">
		<?php if($global->add):?>
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" data-toggle="modal" data-target="#poli" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
				</div>			
			</div>
		<?php endif;?>
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>			
		</div>			
	</div>
	-->
</div>
<?php include 'action.js';?>
<script type="text/javascript">
	setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>