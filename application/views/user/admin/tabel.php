<style type="text/css">
.btn-circle {
    width: 25px;
    height: 25px;
    text-align: center;
    padding: 3px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>
<div class="box box-solid">
        <div class="box-header bg-blue">
          <h3 class="box-title">Tabel <?php echo ucwords($global->headline)?>
          </h3><br><small><?php echo ucwords($subheadline)?></small>
        </div>
        <div class="box-body table-responsive">
        	<table style="width:100%" id="datatabel" class="table table-bordered table-striped">
                <thead>
	                <tr>
	                  <th width="5%">No</th>
	                  <th width="10%">Nama</th>
                      <th width="10%">Username</th>
	                  <th width="15%">Level</th>
                      <th width="10%">Terdaftar</th>
	                  <th width="10%" class="text-center">Aksi</th>
	                </tr>
                </thead>
                <tbody>
                	<?php $i=1;foreach ($data as $row):?>
	                	<tr>
	                		<td><?=$i?></td>
	                		<td><?=ucwords($row->user_nama)?></td>
                            <td><?=ucwords($row->user_username)?></td>
                            <td><?=$row->user_level==1 ? '<label class="label label-success">Admin</label>':($row->user_level==2 ? '<label class="label label-success">Operator</label>':($row->user_level==3 ? '<label class="label label-primary">Tamu</label>':'<label class="label label-danger">Unknown</label>'))?></td>
	                		<td><?=date('d-m-Y',strtotime($row->user_terdaftar))?></td>
	                		<td class="text-center">
	                			<?php include 'button.php';?>
	                		</td>
	                	</tr>	                	
                	<?php $i++;endforeach;?> 
                </tbody>            		
        	</table>
        	<p>Keterengan : <br>
        		<a href="#" class="btn btn-flat btn-xs btn-warning" style="width:25px"><span class="fa fa-eye"></span></a> : Detail<br>
        		<a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
        		<a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus	
        	</p>
        </div>
</div>
<?php include 'action.js'; ?>