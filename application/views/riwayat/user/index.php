<div id="view">
	<div class="row">
		<?php if($global->add):?>
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" data-toggle="modal" data-target="#poli" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
				</div>			
			</div>
		<?php endif;?>
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>			
		</div>			
	</div>
</div>
<?php include 'action.js';?>
<script type="text/javascript">
	setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>