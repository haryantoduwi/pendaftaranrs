<div class="btn-group">	
	<?php if($row->kunjungan_status==1):?>
		<a href="<?=base_url($global->url.'aktivasi/'.$row->kunjungan_id)?>"  class="aktivasi btn btn-flat btn-xs btn-danger <?= $global->aktivasi!=true ? 'disabled':'' ?>"><span class="fa fa-close"></span></a>
	<?php else:?>
		<a href="<?=base_url($global->url.'aktivasi/'.$row->kunjungan_id)?>"  class="aktivasi btn btn-flat btn-xs btn-info <?= $global->aktivasi!=true ? 'disabled':'' ?>"><span class="fa fa-check"></span></a>
	<?php endif?>	
	<a href="<?=base_url($global->url.'suspend/'.$row->kunjungan_id)?>"  class="aktivasi btn btn-flat btn-xs btn-info <?= $global->aktivasi!=true ? 'disabled':'' ?>"><span class="fa fa-close"></span></a>	
	<a href="#" onclick="detail(<?=$row->kunjungan_id?>)" url="<?= base_url($global->url.'detail')?>" class="detail btn btn-flat btn-xs btn-success <?= $global->detail!=true ? 'disabled':'' ?>"><span class="fa fa-eye"></span></a>
	<a href="#"   onclick="edit(<?=$row->kunjungan_id?>)"  url="<?= base_url($global->url.'edit')?>" class="edit btn btn-flat btn-xs btn-warning <?= $global->detail!=true ? 'disabled':'' ?>"><span class="fa fa-pencil"></span></a>
	<a href="#" url="<?=base_url($global->url.'hapus/'.$row->kunjungan_id)?>"  class="hapus btn btn-flat btn-xs btn-danger <?= $global->delete!=true ? 'disabled':'' ?>"><span class="fa fa-trash"></span></a>	

</div>