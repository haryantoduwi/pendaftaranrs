<script type="text/javascript">
  $(document).ready(function(){
    $('.hapus').click(function(){
      var url=$(this).attr('url');
      swal({
        title:'Perhatian',
        text:'Hapus Data',
        html:true,
        ConfirmButtonColor:'#d9534F',
        showCancelButton:true,
        type:'warning'
      },function(){
        window.location.href=url
      });
      return false
    })       
    $('#datatabel').DataTable();
    $('#exporttabel').DataTable({
      responsive: true,
          dom: 'Bfrtip',
          buttons: [
              'excel', 'pdf', 'print'
          ] ,
          pageLength:100,      
      
    });    
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        todayBtn: true,
    });
    $(".selectdata").select2();             
  })
  function add(){
    var url=$("#add").attr('url');   
    $("#view").load(url);      
  }
  function edit(id){   
    var url=$('.edit').attr('url');
    //var id=$(this).attr('id');
    //alert(id);
    $.ajax({
      type:'POST',
      url:url,
      data:{id:id},
      success:function(data){
        $("#view").html(data);       
      }
    })
    return false;
  }
  function detail(id){   
    var url=$('.detail').attr('url');
    //var id=$(this).attr('id');
    //alert(url);
    $.ajax({
      type:'POST',
      url:url,
      data:{id:id},
      success:function(data){
        $("#tabel").html(data);       
      }
    })
    return false;        
  }  
</script>