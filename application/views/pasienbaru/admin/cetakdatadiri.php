<table width="100%">
	<tr>
		<td width="30%" align="left">No RM</td>
		<td width="50%" align="left"><?=$data->pendaftaran_rm?></td>
		<td width="20%" rowspan="6" align="left"><img src="<?= base_url('./asset/dist/img/user.png')?>" alt="User profile picture" style="width:120px;height:120px"></td>
	</tr>
	<tr>
		<td align="left">Nama</td>
		<td align="left"><?= ucwords($data->pendaftaran_nama)?></td>
	</tr>
	<tr>
		<td align="left">Tempat & Tanggal Lahir</td>
		<td align="left"><?= ucwords($data->pendaftaran_tempatlahir).', '.date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?></td>
	</tr>
	<tr>
		<td align="left">No KTP</td>
		<td align="left"><?= ucwords($data->pendaftaran_noktp)?></td>
	</tr>
	<tr>
		<td align="left">No Tlp</td>
		<td align="left"><?= ucwords($data->pendaftaran_notlp)?></td>
	</tr>
	<tr>
		<td align="left">Terdaftar</td>
		<td align="left"><?= date('d-m-Y',strtotime($data->pendaftaran_terdaftar))?></td>
	</tr>
	<tr>
		<td align="left">Alamatp</td>
		<td align="left"><?= ucwords($data->pendaftaran_alamat)?></td>
	</tr>					
</table>	