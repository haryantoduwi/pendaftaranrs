<div id="view">
	<div class="row">
		<div class="col-sm-2">
			<div class="form-group">
				<a href="<?= site_url($global->url.'cetakformulir')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-print"></span> Cetak Formulir</a>				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="box box-solid">
			    <div class="box-body box-profile">
			      <p class="bg-blue" style="padding: 5px">Profil</p>
			      <img class="profile-user-img img-responsive img-circle" src="<?= base_url('./asset/dist/img/user.png')?>" alt="User profile picture">

			      <h3 class="profile-username text-center"><?= ucwords($data->pendaftaran_nama)?></h3>

			      <p class="text-muted text-center"><small class="bg-red">No RM</small> <?= $data->pendaftaran_rm?></p>

			      <ul class="list-group list-group-unbordered">
			        <li class="list-group-item">
			          <b>Tempat & Tgl Lahir</b> <br> <a class=""><?= ucwords($data->pendaftaran_tempatlahir).', '.date('d-m-Y',strtotime($data->pendaftaran_tgllahir))?></a>
			        </li>
			        <li class="list-group-item">
			          <b>No.KTP</b><br><a class=""><?= $data->pendaftaran_noktp?></a>
			        </li>
			        <li class="list-group-item">
			          <b>No.Tlp</b><br><a class=""><?= $data->pendaftaran_notlp?></a>
			        </li>
			        <li class="list-group-item">
			          <b>Terdaftar</b><br><a class=""><?= date('d-m-Y',strtotime($data->pendaftaran_terdaftar))?></a>
			        </li>			        
			      </ul>

			      <a href="#" class="btn btn-flat btn-primary btn-block <?= $data->pendaftaran_status? '':'disabled'?>"><b>Edit Profil</b></a>
			      <p class="help-block text-red <?= $data->pendaftaran_status? 'hide':''?>">Akun anda belum aktif</p>
			    </div>			
			</div>
		</div>
		<div class="col-sm-9">
			<div class="box box-solid">
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped" width="100%">
							<tr>
								<td colspan="2" class="bg-blue" align="left">Data Diri</td>
							</tr>
							<tr>
								<td width="30%" align="left">Alamat</td>
								<td width="70%"><?= ucwords($data->pendaftaran_alamat)?></td>
							</tr>
							<tr>
								<td colspan="2" class="bg-red" align="left">User Sisten</td>
							</tr>
							<tr>
								<td>Username</td>
								<td><?=$data->pendaftaran_username?></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><?=md5($data->pendaftaran_password)?><br><small>*terenkripsi</small></td>
							</tr>														
						</table>
					</div>					
				</div>
			</div>
		</div>		
		<?php //print_r($data)?>
	</div>
</div>
<?php include 'action.js';?>