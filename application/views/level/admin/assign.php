<div class="row">
	<div class="col-sm-12 animated bounceInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form id="formadd" method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Id</label>
								<input readonly type="text" name="id" value="<?= $data->level_id?>" class="form-control">
							</div>
							<div class="form-group">
								<label>Nama</label>
								<input type="text" name="level_nama" readonly class="form-control text-capitalize" value="<?= $data->level_nama?>">
							</div>																	 
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="box box-solid ">
									<div class="box-header bg-blue ">
										<h3 class="box-title"><?= ucwords('Assign Menu')?></h3>
									</div>
									<div class="box-body table-responsive">
										<div id="assign">
										<table class="table table-striped" width="100%" >
											<thead>
												<tr>
													<th width="10%">No</th>
													<th width="90%" colspan="2">Nama</th>
												</tr>
											</thead>
											<tbody>
												<?php $i=1;foreach($menu AS $row):?>
													<tr>
														<td><?= $i?></td>
														<td><?= ucwords($row->menu_nama)?>
														</td>
														<td class="">
															<label>
																<input type="checkbox" class="minimal" name="assign[]" value="<?= $row->menu_id?>" <?= $row->akseslevel? 'checked':''?>>
															</label>
														
														</td>
													</tr>
													<?php if($row->submenu):?>
														
														<?php $x=1;foreach($row->submenu AS $rows):?>
														<tr>
															<td>&nbsp</td>
															<td><b><?= $i.'.'.$x.' '.ucwords($rows->menu_nama)?></b></td>
															<td>
															<label class="text-right">
																<input type="checkbox" class="minimal" name="assignsubmenu[]" value="<?= $rows->menu_id?>" <?= $rows->akseslevel? 'checked':''?>>
															</label>
															</td>
														</tr>
														<?php $x++;endforeach;?>
														
														<?php endif;?>													
												<?php $i++;endforeach;?>
											</tbody>
										</table>
										</div>			
									</div>
								</div>									
							</div>							
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">						
							<div class="form-group">
								<button type="submit" value="submit" name="submit" class="btn btn-warning btn-block btn-flat">Simpan</button>
							</div>														
						</div>
					</div>
				</form>		
			</div>
		</div>		
	</div>	
</div>
<script type="text/javascript">
	//CKEDITOR.replace('editor1');
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    });
	$('#assign').slimScroll({
		height: '300px'
	});    
</script>
<?php include 'action.js'?>