<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= ucwords('Laporan Kunjungan')?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url();?>asset/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url();?>asset/fontawesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>asset/dist/css/AdminLTE.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> 
</head>
<style type="text/css">
/*  @media print {
      #Header, #Footer { display: none !important; }
  }  */
</style>
<body onload="window.print();"> 
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-newpaper-o"></i> Laporan Kunjungan
          <small class="pull-right">Tanggal: <?= date('d-m-Y')?></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
            <table style="width:100%" id="" class="table table-bordered table-striped ">
                  <thead>
                      <tr class="bg-gray">
                        <th width="5%">No</th>
                        <th width="10%">Kode</th>
                        <th width="10%">Pasien</th>
                        <th width="10%">Daftar</th>
                        <th width="15%">Poli</th>
                        <th width="15%">Dokter</th>
                        <th width="15%">Tersimpan</th>
                       
                      </tr>
                  </thead>
                  <tbody>
                    <?php $i=1;$jum=0;foreach ($data as $row):?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=ucwords($row->kunjungan_kode)?><br>
                                  <?php if($row->kunjungan_status==1):?>
                                      <label class="label label-success">Status : Aktif</label>
                                  <?php elseif($row->kunjungan_status==99):?>
                                      <label class="label label-danger">Status : Suspend</label>
                                  <?php else:?>
                                      <label class="label label-primary">Status : Menunggu Aktivasi</label>
                                  <?php endif;?>    
                              </td>
                              <td><?=ucwords($row->pendaftaran_nama)?></td>
                          <td><?=date('d-m-Y',strtotime($row->kunjungan_tanggal))?></td>
                              <td><?=ucwords($row->poli_nama)?></td>
                              <td><?=ucwords($row->dokter_nama)?></td>
                              <td><?=date('d-m-Y',strtotime($row->kunjungan_tersimpan))?></td>
                              <?php
                                 
                                 $jumlah=$global->harga;
                                 $jum+=$jumlah;
                                     
                              ?>                            
                        </tr>                   
                    <?php $i++;endforeach;?> 
                      <tr class="bg-red">
                          <td colspan="6" class="text-right">Jumlah</td>
                          <td class="text-center"><?= $jum?></td>
                      </tr>
                  </tbody>                
            </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


  </section>
</div>
</body>
</html>
