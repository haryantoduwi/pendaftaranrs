<div id="view">
	<div class="row">
		<div class="col-sm-12">
			<div class="box box-primary">

				<div class="box-body">
					<div class="row">
						<form action="<?= base_url($global->url.'cetakkunjungan')?>" method="POST" target="blank">
						<div class="col-sm-2">
							<div class="form-group">
								<label>Tanggal</label>
								<input type="text" name="tglmulai" class="form-control datepicker" value="<?= date('d-m-Y')?>">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>&nbsp</label>
								<input type="text" name="tglselesai" class="form-control datepicker" value="<?= date('d-m-Y')?>">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>&nbsp</label>
								<button type="button" class="btn btn-block btn-flat btn-primary" id="btncari" url="<?= base_url($global->url.'cari')?>" onclick="cari()" >Cari</button>
							</div>
						</div>	
						<div class="col-sm-2 ">
							<div class="form-group">
								<label>&nbsp</label>
								<button type="submit" class="btn btn-block btn-flat btn-warning hide" id="btncetak">Cetak</button>
							</div>
						</div>	
						</form>																	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<!--
		<div class="col-sm-2">
			<div class="form-group">
				<button onclick="add();" id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
		</div>
		-->
		<?php if($global->add):?>
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" data-toggle="modal" data-target="#poli" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
				</div>			
			</div>
		<?php endif;?>
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>			
		</div>			
	</div>

</div>
<?php include 'action.js';?>
<script type="text/javascript">
	// setTimeout(function () {
 //        var url=$('#tabel').attr('url');
 //        $("#tabel").load(url);
 //        //alert(url);
 //    }, 200); 
	function cari(){
		var tglmulai=$('[name=tglmulai]').val();
		var tglselesai=$('[name=tglselesai]').val();
	    var url=$('#btncari').attr('url');
	    $.ajax({
	      type:'POST',
	      url:url,
	      data:{
	      	tglmulai:tglmulai,
	      	tglselesai:tglselesai,
	      },
	      success:function(data){
	        $("#tabel").html(data); 
	        $("#btncetak").removeClass('hide');   
	      }
	    })		
		//alert(url);
	}
</script>