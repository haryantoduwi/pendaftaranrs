<style type="text/css">
.btn-circle {
    width: 25px;
    height: 25px;
    text-align: center;
    padding: 3px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
</style>
<div class="box box-solid">
    <div class="box-header bg-blue">
      <h3 class="box-title">Tabel <?php echo ucwords($global->headline)?>
      </h3><br><small><?= ucwords($subheadline)?></small>
    </div>
    <div class="box-body table-responsive">
    	<table style="width:100%" id="" class="table table-bordered table-striped ">
            <thead>
                <tr class="bg-gray">
                  <th width="5%">No</th>
                  <th width="10%">Kode</th>
                  <th width="10%">Pasien</th>
                  <th width="10%">Daftar</th>
                  <th width="15%">Poli</th>
                  <th width="15%">Dokter</th>
                  <th width="15%">Tersimpan</th>
                 
                </tr>
            </thead>
            <tbody>
            	<?php $i=1;$jum=0;foreach ($data as $row):?>
                	<tr>
                		<td><?=$i?></td>
                		<td><?=ucwords($row->kunjungan_kode)?><br>
                            <?php if($row->kunjungan_status==1):?>
                                <label class="label label-success">Status : Aktif</label>
                            <?php elseif($row->kunjungan_status==99):?>
                                <label class="label label-danger">Status : Suspend</label>
                            <?php else:?>
                                <label class="label label-primary">Status : Menunggu Aktivasi</label>
                            <?php endif;?>    
                        </td>
                        <td><?=ucwords($row->pendaftaran_nama)?></td>
                		<td><?=date('d-m-Y',strtotime($row->kunjungan_tanggal))?></td>
                        <td><?=ucwords($row->poli_nama)?></td>
                        <td><?=ucwords($row->dokter_nama)?></td>
                        <td><?=date('d-m-Y',strtotime($row->kunjungan_tersimpan))?></td>
                        <?php
                           
                           $jumlah=$global->harga;
                           $jum+=$jumlah;
                               
                        ?>                            
                	</tr>	                	
            	<?php $i++;endforeach;?> 
                <tr class="bg-red">
                    <td colspan="6" class="text-right">Jumlah</td>
                    <td class="text-center"><?= $jum?></td>
                </tr>
            </tbody>            		
    	</table>
    </div>
</div>
<?php include 'action.js';?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatabel').DataTable();
    })
</script>