/*
 Navicat Premium Data Transfer

 Source Server         : PHPMYADMIN
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : pendaftaranrs

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 31/05/2019 07:43:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for daftar
-- ----------------------------
DROP TABLE IF EXISTS `daftar`;
CREATE TABLE `daftar`  (
  `daftar_id` int(11) NOT NULL AUTO_INCREMENT,
  `daftar_idpasien` int(255) NULL DEFAULT NULL,
  `daftar_idpoli` int(255) NULL DEFAULT NULL,
  `daftar_tanggal` date NULL DEFAULT NULL,
  `daftar_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `daftar_idjam` int(255) NULL DEFAULT NULL,
  `daftar_tersimpan` date NULL DEFAULT NULL,
  PRIMARY KEY (`daftar_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of daftar
-- ----------------------------
INSERT INTO `daftar` VALUES (1, 2, 1, '2019-04-02', ' bpjs', 7, '2019-04-02');
INSERT INTO `daftar` VALUES (2, 2, 1, '2019-04-03', 'umum', 7, '2019-04-02');
INSERT INTO `daftar` VALUES (3, 2, 2, '2019-04-02', 'umum', 10, '2019-04-02');
INSERT INTO `daftar` VALUES (4, 2, 1, '2019-04-02', 'umum', 8, '2019-04-02');
INSERT INTO `daftar` VALUES (5, 2, 1, '2019-04-02', ' bpjs', 7, '2019-04-02');
INSERT INTO `daftar` VALUES (6, 2, 1, '2019-04-02', ' bpjs', 7, '2019-04-02');
INSERT INTO `daftar` VALUES (7, 2, 1, '2019-04-02', ' bpjs', 7, '2019-04-02');
INSERT INTO `daftar` VALUES (8, 2, 1, '2019-04-04', ' bpjs', 7, '2019-04-04');
INSERT INTO `daftar` VALUES (9, 2, 1, '2019-04-02', ' bpjs', 7, '2019-04-04');
INSERT INTO `daftar` VALUES (10, 2, 1, '2019-04-15', NULL, 2, '2019-04-15');
INSERT INTO `daftar` VALUES (11, 2, 1, '2019-04-15', NULL, 6, '2019-04-15');
INSERT INTO `daftar` VALUES (12, 2, 1, '2019-04-15', NULL, 2, '2019-04-15');
INSERT INTO `daftar` VALUES (13, 2, 1, '2019-04-15', NULL, 2, '2019-04-15');

-- ----------------------------
-- Table structure for dokter
-- ----------------------------
DROP TABLE IF EXISTS `dokter`;
CREATE TABLE `dokter`  (
  `dokter_id` int(11) NOT NULL AUTO_INCREMENT,
  `dokter_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dokter_jeniskelamin` bit(1) NULL DEFAULT NULL,
  `dokter_nik` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dokter_tempatlahir` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `dokter_tgllahir` date NULL DEFAULT NULL,
  `dokter_alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dokter_idpoli` int(255) NULL DEFAULT NULL,
  `dokter_idjampraktek` int(255) NULL DEFAULT NULL,
  `dokter_notlp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dokter_foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`dokter_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dokter
-- ----------------------------
INSERT INTO `dokter` VALUES (2, 'duwi', b'1', '12222', 'bantul', '2019-03-25', 'bantul', 1, 7, '08585456455', '469ec14db890496862adc1e7b80d965e.png');
INSERT INTO `dokter` VALUES (3, 'Pedrosa adtyo prabowo sulistyawan', b'1', '123132132', 'Bantul', '1991-04-11', 'Bantul', 2, 8, '085725818424', '2aabdf1b4b4fe386178e8fd11c81bb93.png');
INSERT INTO `dokter` VALUES (4, 'Ani', b'1', '111111111111', 'Yogyakarta', '1989-04-11', 'Yogyakarta', 3, 10, '085745456', '0469a17d452041c2f2fd5e74e72d94c7.jpg');
INSERT INTO `dokter` VALUES (6, 'alesandro', b'1', '1111111', 'bantul', '1980-07-24', 'bantul', 1, 7, '08572581844', '8bd422a442e8e4d460566012eb195ee6.png');

-- ----------------------------
-- Table structure for jampraktek
-- ----------------------------
DROP TABLE IF EXISTS `jampraktek`;
CREATE TABLE `jampraktek`  (
  `jampraktek_id` int(11) NOT NULL AUTO_INCREMENT,
  `jampraktek_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jampraktek_mulai` time(0) NULL DEFAULT NULL,
  `jampraktek_selesai` time(0) NULL DEFAULT NULL,
  `jampraktek_status` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`jampraktek_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jampraktek
-- ----------------------------
INSERT INTO `jampraktek` VALUES (7, 'Pagi', '08:03:00', '11:03:00', b'1');
INSERT INTO `jampraktek` VALUES (8, 'Siang', '08:30:00', '11:03:00', b'1');
INSERT INTO `jampraktek` VALUES (10, 'Sore', '07:04:00', '11:04:00', b'1');

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan`  (
  `kegiatan_id` int(255) NOT NULL AUTO_INCREMENT,
  `kegiatan_nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kegiatan_date` date NULL DEFAULT NULL,
  `kegiatan_file` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`kegiatan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kegiatan
-- ----------------------------
INSERT INTO `kegiatan` VALUES (61, 'adwd', 'teloo', '2018-11-24', 'cf025eb64962066f05f249398d1886dd.pdf');
INSERT INTO `kegiatan` VALUES (73, 'hhh', 'hhh', '2018-11-24', NULL);
INSERT INTO `kegiatan` VALUES (74, 'heloo', 'hello', '2018-11-25', '3198a2947b6e1e8433ff5c584bb05c22.pdf');
INSERT INTO `kegiatan` VALUES (77, 'dada', 'dawdawss', '2019-02-25', NULL);

-- ----------------------------
-- Table structure for kunjunganpasien
-- ----------------------------
DROP TABLE IF EXISTS `kunjunganpasien`;
CREATE TABLE `kunjunganpasien`  (
  `kunjungan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kunjungan_kode` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kunjungan_idpasien` int(255) NULL DEFAULT NULL,
  `kunjungan_tanggal` date NULL DEFAULT NULL,
  `kunjungan_iddokter` int(255) NULL DEFAULT NULL,
  `kunjungan_status` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kunjungan_tersimpan` date NULL DEFAULT NULL,
  `kunjungan_antrian_status` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kunjungan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kunjunganpasien
-- ----------------------------
INSERT INTO `kunjunganpasien` VALUES (3, 'D2190001', 2, '2019-05-24', 2, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (5, 'D4190002', 2, '2019-04-17', 4, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (6, 'D4190003', 2, '2019-04-18', 4, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (7, 'D2190004', 2, '2019-04-17', 2, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (8, 'D6190005', 6, '2019-05-24', 6, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (9, 'D2190006', 2, '2019-04-20', 2, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (10, 'D2190007', 2, '2019-04-17', 2, '0', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (11, 'D2190008', 2, '2019-04-17', 2, '99', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (12, 'D2190009', 2, '2019-04-19', 2, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (16, 'D6190013', 2, '2019-05-22', 6, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (17, 'D2190014', 2, '2019-05-24', 2, '1', '2019-04-17', '4');
INSERT INTO `kunjunganpasien` VALUES (18, 'D2190015', 5, '2019-05-24', 2, '1', '2019-04-20', '4');
INSERT INTO `kunjunganpasien` VALUES (20, 'D4190016', 2, '2019-04-24', 4, '1', '2019-04-21', '4');
INSERT INTO `kunjunganpasien` VALUES (21, 'D6190017', 2, '2019-04-27', 6, '0', '2019-04-24', '4');

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS `level`;
CREATE TABLE `level`  (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level_created_at` date NULL DEFAULT NULL,
  `level_update_at` date NULL DEFAULT NULL,
  `level_keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`level_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of level
-- ----------------------------
INSERT INTO `level` VALUES (1, 'administrator', '2019-05-27', NULL, '-');
INSERT INTO `level` VALUES (2, 'operator', '2019-05-27', NULL, '-');
INSERT INTO `level` VALUES (3, 'guest', '2019-05-27', NULL, '-');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_ikon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_is_mainmenu` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_link` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_akses_level` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_urutan` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_status` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 'master', 'fa fa-gears', '0', 'master/admin', '1,2,3', '2', '1');
INSERT INTO `menu` VALUES (2, 'barang', 'fa fa-circle-o', '1', 'barang/admin', '1,1', '1', '0');
INSERT INTO `menu` VALUES (4, 'home', 'fa fa-home', '0', 'frontend/home', '0', '2', '1');
INSERT INTO `menu` VALUES (5, 'download', 'fa fa-download', '0', 'frontend/download', '0', '4', '0');
INSERT INTO `menu` VALUES (6, 'kegiatan', 'fa fa-tasks', '0', 'frontend/kegiatan', '0', '3', '0');
INSERT INTO `menu` VALUES (12, 'pendaftaran', 'fa  fa-book', '0', 'frontend/pendaftaran', '0', '4', '1');
INSERT INTO `menu` VALUES (13, 'data diri', 'fa fa-user', '0', 'datadiri/user', '99', '98', '1');
INSERT INTO `menu` VALUES (14, 'password', 'fa fa-lock', '0', 'password/user', '99', '99', '0');
INSERT INTO `menu` VALUES (15, 'riwayat', 'fa fa-exchange', '0', 'riwayat/user', '99', '2', '1');
INSERT INTO `menu` VALUES (16, 'dashboard', 'fa fa-dashboard', '0', 'dashboard/user', '99', '1', '1');
INSERT INTO `menu` VALUES (17, 'pasien', 'fa fa-users', '0', 'dashboard/admin', '1,2', '3', '1');
INSERT INTO `menu` VALUES (18, 'baru', 'fa fa-user', '17', 'pasienbaru/admin', '1,2', '1', '1');
INSERT INTO `menu` VALUES (19, 'dashboard', 'fa fa-dashboard', '0', 'dashboard/admin', '1,2', '1', '1');
INSERT INTO `menu` VALUES (20, 'terdaftar', 'fa fa-users', '17', 'terdaftar/admin', '1,2', '2', '1');
INSERT INTO `menu` VALUES (21, 'dokter', 'fa fa-user', '1', 'dokter/admin', '1,2,3', '2', '1');
INSERT INTO `menu` VALUES (22, 'pegawai', 'fa fa-user', '1', 'pegawai/admin', '1,2', '3', '0');
INSERT INTO `menu` VALUES (23, 'poli', 'fa fa-tasks', '1', 'poli/admin', '1,2,3', '1', '1');
INSERT INTO `menu` VALUES (24, 'obat', 'fa fa-heart', '0', '#', '1', '4', '0');
INSERT INTO `menu` VALUES (25, 'stok', 'fa fa-circle-o', '24', 'stokobat/admin', '1', '1', '1');
INSERT INTO `menu` VALUES (26, 'pengadaan', 'fa  fa-circle-o', '24', 'pengadaanobat/admin', '1', '2', '1');
INSERT INTO `menu` VALUES (27, 'pengeluaran', 'fa  fa-circle-o', '24', 'pengeluaranobat/admin', '1', '3', '1');
INSERT INTO `menu` VALUES (28, 'obat', 'fa  fa-circle-o', '24 ', 'obat/admin', '1', '4', '1');
INSERT INTO `menu` VALUES (29, 'tindakan', 'fa fa-bed', '0', 'tindakan/admin', '1', '5', '0');
INSERT INTO `menu` VALUES (30, 'log out', 'fa fa-sign-out', '0', 'login/logout', '1,2', '99', '1');
INSERT INTO `menu` VALUES (31, 'jam praktek', 'fa fa-circle-o', '1', 'jampraktek/admin', '1,2,3', '4', '1');
INSERT INTO `menu` VALUES (32, 'kunjungan', 'fa fa-tasks', '0', 'kunjungan/admin', '1,2', '6', '1');
INSERT INTO `menu` VALUES (34, 'pasien baru', 'fa fa-circle-o', '12', 'frontend/pendaftaran', '0', '1', '1');
INSERT INTO `menu` VALUES (35, 'poli', 'fa fa-circle-o', '12', 'frontend/pendaftaranpoli', '0', '2', '1');
INSERT INTO `menu` VALUES (36, 'laporan', 'fa fa-print', '0', '#', '1,3', '6', '1');
INSERT INTO `menu` VALUES (37, 'kunjungan', 'fa fa-circle-o', '36', 'laporankunjungan/admin', '1,3', '1', '1');
INSERT INTO `menu` VALUES (38, 'setting', 'fa fa-gears', '0', '#', '1', '7', '1');
INSERT INTO `menu` VALUES (39, 'user', 'fa fa-circle-o', '38', 'user/admin', '1,3', '2', '1');
INSERT INTO `menu` VALUES (40, 'antrian', 'fa  fa-random', '0', 'antrian/admin', '1,2', '4', '1');
INSERT INTO `menu` VALUES (42, 'antrian', 'fa fa-retweet', '0', 'frontend/antrian', '0', '5', '1');
INSERT INTO `menu` VALUES (43, 'level', 'fa fa-circle-o', '38', 'level/admin', '1', '1', '1');

-- ----------------------------
-- Table structure for pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `pendaftaran`;
CREATE TABLE `pendaftaran`  (
  `pendaftaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pendaftaran_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendaftaran_tempatlahir` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_tgllahir` date NULL DEFAULT NULL,
  `pendaftaran_noktp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_notlp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendaftaran_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_rm` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendaftaran_username` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_status` bit(1) NULL DEFAULT NULL,
  `pendaftaran_param` bit(1) NULL DEFAULT NULL,
  `pendaftaran_terdaftar` date NULL DEFAULT NULL,
  `pendaftaran_jeniskelamin` bit(1) NULL DEFAULT NULL,
  `pendaftaran_foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_ktp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pendaftaran_namawali` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pendaftaran_alamatwali` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pendaftaran_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pendaftaran
-- ----------------------------
INSERT INTO `pendaftaran` VALUES (1, 'duwi haryanto', 'bantul', '1991-02-07', '0245456454', '08757576778', 'bantul', '19RM0001', 'admin', 'admin', b'1', NULL, '2019-02-28', b'1', NULL, NULL, NULL, NULL);
INSERT INTO `pendaftaran` VALUES (2, 'Dani Pedrosa', 'bantul', '1991-03-07', '1787898799999', '085725818424', 'Pedak Wijirjeo Pandak Bantul', 'RM190002', 'dani', 'dani', b'1', NULL, '2019-04-20', b'1', '37c2bd46ef56c8a8da26e1a8f982e9c0.jpg', '6311c472a176637f068b997f9bc91056.jpg', 'sarjiyem', 'bantul');
INSERT INTO `pendaftaran` VALUES (5, 'Duwi Haryanto', 'bantul', '1991-03-07', '1787898799999', '085725818424', 'Pedak Wijijreo Pandak Bantul', 'RM190003', 'duwi', 'duwi', b'0', NULL, '2019-04-20', b'1', '7a1d05a404a48e61f4e6856bc84654b6.jpg', 'df780e3094584f8f4325aa8d878bdd19.png', 'sarjiyem', 'pedak wijirejo pandak bantul');
INSERT INTO `pendaftaran` VALUES (6, 'agus', 'bantul', '1990-02-22', '111111111111111111111', '085725818424', 'bantul', 'RM190004', 'agus', 'agus', b'1', NULL, '2019-05-06', b'1', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for poli
-- ----------------------------
DROP TABLE IF EXISTS `poli`;
CREATE TABLE `poli`  (
  `poli_id` int(11) NOT NULL AUTO_INCREMENT,
  `poli_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poli_ruangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `poli_tersimpan` date NULL DEFAULT NULL,
  `poli_kuota` int(100) NULL DEFAULT NULL,
  PRIMARY KEY (`poli_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of poli
-- ----------------------------
INSERT INTO `poli` VALUES (1, 'Gigi', 'Ruang S1001', '2019-03-26', 100);
INSERT INTO `poli` VALUES (2, 'Umum', 'S1002', '2019-03-27', 100);
INSERT INTO `poli` VALUES (3, 'Kehamilan', 'S1005', '2019-04-02', 90);
INSERT INTO `poli` VALUES (5, 'THT', 'S2001', '2019-04-02', 90);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_level` int(2) NULL DEFAULT NULL,
  `user_terdaftar` date NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 'admina', 1, '2018-09-29');
INSERT INTO `user` VALUES (3, 'haryanto', 'haryanto', 'haryanto duwi', 2, '2018-10-21');
INSERT INTO `user` VALUES (4, 'zahid', 'zahid', 'zahid', 2, '2019-05-08');
INSERT INTO `user` VALUES (5, 'tamu', 'tamu', 'tamu', 3, '2019-05-31');

SET FOREIGN_KEY_CHECKS = 1;
